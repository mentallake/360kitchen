var downloadTable;

$(function(){
	if($('.data-table').length > 0){
		downloadTable = $('.data-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": site_url + "admin/Download/data_processing",
                "type": "POST",
                "data": function(d){
                    // d.status = $('#status-select').length > 0 ? $('#status-select').selectpicker('val') : ""
                }
            },
            "columns": [
                { "data": "id",
                  "render": function(data){
                    return '<input type="checkbox" class="item-checkbox" value="' + data + '">';
                  }
                },
                { "data": "no" },
                { "data": "title" },
                { "data": "issue_date" },
                { "data": "status_text" },
                { "data": "created_time" },
                { "data": "updated_time" },
                { "data": "id",
                  "render": function(data){
                    var content = '<a href="' + site_url + 'admin/Download/edit/' + data + '" title="แก้ไขข้อมูล" class="action-btn edit-btn"><i class="fa fa-pencil"></i></a> \
                    <a href="' + site_url + 'admin/Download/delete/' + data + '" title="ลบ" class="action-btn delete-btn"><i class="fa fa-trash-o"></i></a>';

                    return content;
                  }
                }
            ],
            "createdRow": function( row, data, dataIndex ){
                $(row).attr('data-id', data['id']);
                $(row).attr('class', data['status_class']);
            },
            columnDefs: [ {
                orderable: false,
                //className: 'select-checkbox',
                targets: 0
            }, {
                orderable: false,
                targets: 1,
            }, {
                orderable: true,
                className: 'status-col',
                targets: 4,
            }, {
                orderable: false,
                className: 'action-col text-right',
                targets: 7
            } ],
            order: [[ 2, 'asc' ]],
			"language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Thai.json"
            },
			"initComplete": function( settings, json ) {
		    	$('[class*="col-sm-"]').each(function(i, el) {
		    		var classes = $(this).attr('class');
		    		var classes = classes.replace(/col-sm-/, "col-xs-");
		    		$(this).attr('class', classes);
				});

                this.api().columns().every( function () {
                    var column = this;
                    var index = column.index();
                } );

                $('select').selectpicker({
                    size: 7
                });
		  	}
		});
	}
});