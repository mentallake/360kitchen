$(function(){
	$(".datepicker").datepicker({
		format: 'dd/mm/yyyy',
		language: 'th-TH'
	});

	$('#download-form').submit(function(){
		if(!isFieldCompleted("#download-form")){
			return false
		}

		showLoadingPanel();

        return true;
	});

	$(".file-upload-wrapper input[type='file']").change(function(){
        readURL(this);
        $(this).parent().addClass('has-image');
    });

	$('#download-form').submit(function(){
		showLoadingPanel();

		if(isFieldCompleted("#download-form")){
			var currentUrl = window.location.href;
			var options = {
	            success: downloadFormResponse,
	            data: { current_url : currentUrl },
	            dataType: 'json'
	        };

	        $(this).ajaxSubmit(options);
		}else{
			hideLoadingPanel();
		}

        return false;
	});
});

function downloadFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        $('#message-dialog').on('hidden.bs.modal', function(){
        	$('[name="' + response.field_name + '"]').focus();
        });

        $('#message-dialog .modal-title').html('Message');
        $('#message-dialog .modal-body').html(message);

        hideLoadingPanel();

    	// Display message dialog
        $('#message-dialog').modal();
    }else{
    	var redirectLink = response.redirect_link;

        window.location.href = response.redirect_link;
    }
}