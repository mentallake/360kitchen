$(function(){
	$('#kitchen-form').submit(function(){
		if(!isFieldCompleted("#kitchen-form")){
			return false
		}

		showLoadingPanel();

        return true;
	});

	$("#gallery-image-list").sortable({
        items: "li.gallery-image-item",
        cursor: "move",
        connectWith: "#gallery-image-list",
        placeholder: "ui-state-highlight col-md-3 col-sm-4",
        start: function(event, ui) {
        },
        change: function(event, ui) {
        },
        update: function(event, ui) {
        },
        stop: function(event, ui){
            updateOrder();
        }
    });

	$("#kitchen-image-upload-btn .file-upload-wrapper input[type='file']").change(function(){
        readURL(this);
        $(this).parent().addClass('has-image');
    });

    $("#add-image-btn input[type='file']").change(handleFileUpload);

    $('#gallery-image-list').on('click', '.gallery-image .remove-image', function(){
		var $parent = $(this).parents('li');
		var imageId = $parent.attr('data-id');

		if(imageId){
			var removeIds = $('#remove-image-ids').val();
			var removeIds = removeIds.trim() == '' ? imageId : removeIds + ',' + imageId;
			$('#remove-image-ids').val(removeIds);
		}

        $(this).parents('li').remove();
    });

	$('#kitchen-form').submit(function(){
		showLoadingPanel();

		if(isFieldCompleted("#kitchen-form")){
			updateOrder();

			var $image = $('#gallery-image-list > li[data-number]');
	        var lastImageNumber = $image.length > 0 ? parseInt($image.last().attr('data-number')) : 0;
	        var totalImages = $('#gallery-image-list > li.gallery-image-item').length;

	        // Make editor to save to textarea value
        	tinymce.triggerSave();

			var currentUrl = window.location.href;
			var options = {
	            success: kitchenFormResponse,
	            data: {
	            	current_url : currentUrl,
	            	last_image_number : lastImageNumber,
                	total_images : totalImages
	            },
	            dataType: 'json'
	        };

	        $(this).ajaxSubmit(options);
		}else{
			hideLoadingPanel();
		}

        return false;
	});
});

function updateOrder(){
    $('#gallery-image-list li.gallery-image-item').each(function(){
        var index = $(this).index() - 1;
        $(this).find('.gi-order-field').val(index);
    });
}

function kitchenFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        $('#message-dialog').on('hidden.bs.modal', function(){
        	$('[name="' + response.field_name + '"]').focus();
        });

        $('#message-dialog .modal-title').html('Message');
        $('#message-dialog .modal-body').html(message);

        hideLoadingPanel();

    	// Display message dialog
        $('#message-dialog').modal();
    }else{
    	var redirectLink = response.redirect_link;

        window.location.href = response.redirect_link;
    }
}

function handleFileUpload(){
    var $inputFile = $("#add-image-btn input[type='file']")[0];
    var files = $inputFile.files;
    var $list = $('#gallery-image-list');

    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        // Only pics
        if (!file.type.match('image'))
            continue;

        var reader = new FileReader();
        reader.addEventListener("load", function (event) {
            var lastNumber = $list.find('li[data-number]').last().attr('data-number');
            lastNumber = $list.find('li[data-number]').length <= 0 ? 0 : parseInt(lastNumber);
            var number = (lastNumber + 1);
            var image = event.target.result;
            var bgImage = 'background-image: url(' + image + ')';
            var orderNo = $('.gallery-image-item').length;
            var html = '<li class="col-sm-3 col-md-3 gallery-image-item" data-number="' + number + '"> \
                            <input type="hidden" name="gallery_image_' + number + '" value="' + image + '"> \
                            <input type="hidden" class="gi-order-field" name="gi_order_' + number + '" value="' + orderNo + '"> \
                            <div class="gallery-image"> \
                                <a class="remove-image btn skelleton"><i class="fa fa-trash"></i></a> \
                                <div class="image-container" style="' + bgImage + '"></div> \
                            </div> \
                        </li>';
            $list.append(html);
        });

        //Read the image
        reader.readAsDataURL(file);
    }

    var $addButton = $('#gallery-image-list #add-image-btn');
    $(this).remove();
    $('<input type="file" class="file-upload" name="gallery_images[]" accept="image/*" multiple>').change(handleFileUpload).appendTo($addButton);
}