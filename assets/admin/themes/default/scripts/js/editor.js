$(function(){
	tinymce.init({
		theme: 'modern',
		selector: 'textarea.editor',
		plugins: 'link lists textcolor',
		menubar: false,
		statusbar: false,
		toolbar1: 'bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent',
	});
});