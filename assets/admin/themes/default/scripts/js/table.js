$(function(){
    $('.all-items-checkbox').change(function(){
        if($(this).prop('checked')){
            $('table td .item-checkbox').prop('checked', true);
        }else{
            $('table td .item-checkbox').prop('checked', false);
        }
    });

    $(document).on('change', 'table .item-checkbox', function(){
        if($(this).prop('checked')){
            var totalItems = $('table .item-checkbox').length;
            var totalChecked = $('table .item-checkbox:checked').length;

            if(totalChecked == totalItems){
                $('.all-items-checkbox').prop('checked', true);
            }
        }else{
            $('.all-items-checkbox').prop('checked', false);
        }
    });

	$(document).on('click', '.action-btn.delete-btn', function(){
		var postUrl = $(this).attr('href');
		var title = 'ยืนยันการลบข้อมูล';
		var text = 'คุณต้องการลบข้อมูลนี้ใช่หรือไม่?';

		$('#confirm-dialog .modal-body').html(text);
		$('#confirm-dialog .modal-title').html(title);

        $('#confirm-dialog #confirm-button').unbind('click');

		$('#confirm-dialog #confirm-button').click(function(){
			$('#confirm-dialog').modal('hide');

			showLoadingPanel();

			$.ajax({
                method: "POST",
                url: postUrl,
                dataType: "json"
            }).done(function(response){
                if(response.result == false){
                    var message = response.message;

                    $('#message-dialog .modal-body').html(message);
                    hideLoadingPanel();
                    $('#message-dialog').modal();
                }else{
                	var removedId = response.updated_id;

                    $('table tr[data-id="' + removedId + '"]').remove();

                    hideLoadingPanel();

                    // Refresh table
                    var tableId = $('table').attr('id');

                    if(tableId == "kitchen-table"){
                        kitchenTable.ajax.reload( null, false );
                    }else if(tableId == "specification-table"){
                        specTable.ajax.reload( null, false );
                    }else if(tableId == "accessaries-table"){
                        accessariesTable.ajax.reload( null, false );
                    }else if(tableId == "download-table"){
                        downloadTable.ajax.reload( null, false );
                    }
                }
            });
	    });

	    $('#confirm-dialog').modal();

	    return false;
	});
});