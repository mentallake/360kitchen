var lastScroll = 0;
var offset = 2;
var blankSpace = 180;
var mobileBlankSpace = 0;
var currentScrollIndex = 0;
var lastScrollIndex = 0;

$('body').addClass('home-page');

$(function(){
	$(window).resize(function(){
		updateSlidePosition();
	});

	if($('#drag-mask').length > 0){
		$('#drag-mask').click(function(){
			$(this).fadeOut();
		});
	}

	setTimeout(function(){
		$('#drag-mask').click();
	}, 2500);

	$('#header-panel .navbar-brand').click(function(){
		$('.detail-mask-panel').animateCss('slideOutDown', 'out', function(){
			$('.detail-mask-panel').removeClass('animated slideOutDown');
			$('.detail-mask-panel').css('opacity', 1);
		});

		$('body').removeClass('show-detail');

		return false;
	});

	initSlidePaging();
	initSlideDetail();

	$('.nav-button').click(function(){
		var $currentSlide = $("#main-slide > section.current");
		var currentSlideIndex = $("#main-slide > section").index($currentSlide);

		if($(this).hasClass('next')){
			var targetIndex = currentSlideIndex + 1;
			goToSlide(targetIndex, 'next');
		}else{
			var targetIndex = currentSlideIndex - 1;
			goToSlide(targetIndex, 'prev');
		}
	});

	$(window).on('load', function(){
		goToSlide(0, 'next');
	});

	$('#main-slide > section').horizon({
		fnCallback: function (i) {
			var totalSlides = $("#main-slide > section").length;
			var $currentSlide = $("#main-slide > section.current");
			var currentIndex = $('#main-slide > section').index($currentSlide);
			var toIndex = currentIndex;
			var direction = 'next';

			if($('body').hasClass('show-detail')){
				lastScrollIndex = i;
				return;
			}

			if(lastScrollIndex == totalSlides - 1){
				if(i == 0){
					// Scroll down
					toIndex++;
				}else{
					// Scrool up
					toIndex--;
					direction = 'prev';
				}
			}else if(lastScrollIndex == 0){
				if(i == totalSlides - 1){
					// Scroll up
					toIndex--;
					direction = 'prev';
				}else{
					// Scrool down
					toIndex++;
				}
			}else{
				if(i > lastScrollIndex){
					// Scroll down
					toIndex++;
				}else if(i < lastScrollIndex){
					// Scrool up
					toIndex--;
					direction = 'prev';
				}
			}

			lastScrollIndex = i;
			currentScrollIndex = i;

			goToSlide(toIndex, direction);

			// if(Math.abs(currentScrollIndex - i) > 1){
			// 	// Current is last slide, next is the first one (Loop).
			// 	if(currentScrollIndex == totalSlides - 1 && i == 0){
			// 		goToSlide(currentIndex + 1, 'next');
			// 	}else{
			// 		goToSlide(currentIndex - 1, 'prev');
			// 	}
			// }else{
			// 	if(i < currentScrollIndex){
			// 		goToSlide(currentIndex - 1, 'prev');
			// 	}else{
			// 		goToSlide(currentIndex + 1, 'next');
			// 	}
			// }

			// currentScrollIndex = i;
		}
	});
});

function initSlidePaging(){
	var windowWidth = $(window).width();

	if(windowWidth < 768){
		var totalSlides = $('#slide-paging > li').length;
		$('#slide-paging > li').css('width', (100 / totalSlides) + '%');
	}else{
		var totalSlides = $('#slide-paging > li').length;
		$('#slide-paging > li').css('height', (100 / totalSlides) + '%');
	}

	$(window).resize(function(){
		var windowWidth = $(window).width();

		if(windowWidth < 768){
			var totalSlides = $('#slide-paging > li').length;
			$('#slide-paging > li').css('width', (100 / totalSlides) + '%');
		}else{
			var totalSlides = $('#slide-paging > li').length;
			$('#slide-paging > li').css('height', (100 / totalSlides) + '%');
		}
	});

	$('#slide-paging > li > a').click(function(){
		var totalSlides = $("#main-slide > section").length;
		var $currentSlide = $("#main-slide > section.current");
		var currentIndex = $('#main-slide > section').index($currentSlide);
		var targetSlideNo = $(this).attr('data-slide-no');
		var $target = $('#main-slide > section[data-slide-no=' + targetSlideNo + ']');
		var targetIndex = $('#main-slide > section').index($target);

		if(Math.abs(currentIndex - targetIndex) > 1){
			if(currentIndex == totalSlides - 1 && targetIndex == 0){
				goToSlide(targetIndex, 'next');
			}else{
				goToSlide(targetIndex, 'prev');
			}
		}else{
			if(targetIndex < currentIndex){
				goToSlide(targetIndex, 'prev');
			}else{
				goToSlide(targetIndex, 'next');
			}
		}
	});
}

function initSlideDetail(){
	$('.detail-btn').click(function(){
		// var url = $(this).attr('href');
		$('body').addClass('show-detail');

		var kitchenId = $(this).attr('data-kitchen-id');

		$('#image-dialog .kitchen-images').addClass('hide');

		$('.detail-mask-panel[data-kitchen-id=' + kitchenId + ']').animateCss('slideInUp', 'in', function(){
			// window.location.href = url;
			// return true;
			$('.detail-mask-panel[data-kitchen-id=' + kitchenId + ']').removeClass('hide');
			$('#image-dialog .kitchen-images[data-kitchen-id=' + kitchenId + ']').removeClass('hide');
		});

		return false;
	});
}

function updateSlidePosition(direction){
	var totalSlides = $("#main-slide > section").length;
	var $currentSlide = $("#main-slide > section.current");
	var currentIndex = $('#main-slide > section').index($currentSlide);
	var currentSlideNo = parseInt($("#main-slide > section.current").attr('data-slide-no'));
	var windowWidth = $(window).width();

	if(windowWidth < 768){
		$('#main-slide > section').each(function(){
			var index = $('#main-slide > section').index(this);

			if(index < currentIndex){
				var offsetIndex = currentIndex - index;
				var offset = 0 - (offsetIndex * Math.ceil(($(window).width() - mobileBlankSpace)));

				offset = 0 - (offsetIndex * Math.ceil(($(window).width() - mobileBlankSpace) - mobileBlankSpace));

				$(this).css({
				    transform: "matrix(1, 0, 0, 1, " + offset + ", 0)",
				    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
				});
			}else if(index == currentIndex){
				$(this).css({
				    transform: "matrix(1, 0, 0, 1, " + mobileBlankSpace + ", 0)",
				    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
				});

				$(this).find('.slide-logo').animateCss('fadeIn', 'in');
				$(this).find('.slide-title').animateCss('slideInLeft', 'in');
				$(this).find('.slide-description').animateCss('slideInLeft', 'in');
				// $(this).find('.detail-btn').animateCss('slideInUp', 'in');
			}else{
				var offsetIndex = index - currentIndex;

				if(direction == 'next'){
					var offset2 = (offsetIndex * Math.ceil(($(window).width() - mobileBlankSpace)));

					$(this).css({
					    transform: "matrix(1, 0, 0, 1, " + offset2 + ", 0)",
		            	transition: "all 0s cubic-bezier(.77, 0, .175, 1)"
					});
				}

				if(direction == 'prev'){
					$(this).css({
					    transform: "matrix(1, 0, 0, 1, " + windowWidth + ", 0)",
					    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
					});
				}

				$(this).find('.mask').animateCss('fadeIn', '');

				var maskTitleOffset = windowWidth - Math.abs(offset) - mobileBlankSpace - mobileBlankSpace;
				$(this).find('.mask-title').css('margin-right', maskTitleOffset + 'px');

				$(this).find('.mask-title').css('top', $currentSlide.find('.slide-title').offset().top + 'px');
			}
		});

		// Update extra slide
		$('#main-slide > .mobile-extra-slide > section').each(function(){
			var index = $('#main-slide > .mobile-extra-slide > section').index(this);
			var offsetTop = 0;

			if(index < currentIndex){
				if(direction == 'prev'){
					$(this).css({
					    transform: "matrix(1, 0, 0, 1, " + windowWidth + ", " + offsetTop + ")",
					    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
					});
				}else{
					$(this).css({
					    transform: "matrix(1, 0, 0, 1, " + windowWidth + ", " + offsetTop + ")",
					    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
					});
				}
			}else if(index == currentIndex){
				$(this).css({
				    transform: "matrix(1, 0, 0, 1, " + mobileBlankSpace + ", " + offsetTop + ")",
				    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
				});
			}else{
				var offsetIndex = index - currentIndex;
				// var offset = 0 - (offsetIndex * Math.ceil(($(window).width() / 2) + mobileBlankSpace));
				var offset = 0 - (offsetIndex * Math.ceil(($(window).width() - mobileBlankSpace)));

				if(direction == 'next'){
					var offset2 = 0 - (offsetIndex * Math.ceil(($(window).width() - mobileBlankSpace)));

					$(this).css({
					    transform: "matrix(1, 0, 0, 1, " + offset2 + ", " + offsetTop + ")",
		            	transition: "all 0s cubic-bezier(.77, 0, .175, 1)"
					});
				}

				offset = 0 - (offsetIndex * Math.ceil(($(window).width() - mobileBlankSpace) - mobileBlankSpace));
				// offset = 0 - (offsetIndex * Math.ceil(($(window).width() / 2) + mobileBlankSpace));

				$(this).css({
				    transform: "matrix(1, 0, 0, 1, " + offset + ", " + offsetTop + ")",
				    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
				});

				$(this).find('.mask').animateCss('fadeIn', '');
			}
		});
	}else{
		$('#main-slide > section').each(function(){
			var index = $('#main-slide > section').index(this);

			if(index < currentIndex){
				if(direction == 'prev'){
					$(this).css({
					    transform: "matrix(1, 0, 0, 1, " + windowWidth + ", 0)",
					    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
					});
				}else{
					$(this).css({
					    transform: "matrix(1, 0, 0, 1, " + windowWidth + ", 0)",
					    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
					});
				}
			}else if(index == currentIndex){
				$(this).css({
				    transform: "matrix(1, 0, 0, 1, " + blankSpace + ", 0)",
				    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
				});

				$(this).find('.slide-logo').animateCss('fadeIn', 'in');
				$(this).find('.slide-title').animateCss('slideInLeft', 'in');
				$(this).find('.slide-description').animateCss('slideInLeft', 'in');
				$(this).find('.detail-btn').animateCss('slideInUp', 'in');
			}else{
				var offsetIndex = index - currentIndex;
				// var offset = 0 - (offsetIndex * Math.ceil(($(window).width() / 2) + blankSpace));
				var offset = 0 - (offsetIndex * Math.ceil(($(window).width() - blankSpace)));

				if(direction == 'next'){
					var offset2 = 0 - (offsetIndex * Math.ceil(($(window).width() - blankSpace)));

					$(this).css({
					    transform: "matrix(1, 0, 0, 1, " + offset2 + ", 0)",
		            	transition: "all 0s cubic-bezier(.77, 0, .175, 1)"
					});
				}

				offset = 0 - (offsetIndex * Math.ceil(($(window).width() - blankSpace) - blankSpace));
				// offset = 0 - (offsetIndex * Math.ceil(($(window).width() / 2) + blankSpace));

				$(this).css({
				    transform: "matrix(1, 0, 0, 1, " + offset + ", 0)",
				    transition: "all 1s cubic-bezier(.77, 0, .175, 1)"
				});

				$(this).find('.mask').animateCss('fadeIn', '');

				var maskTitleOffset = windowWidth - Math.abs(offset) - blankSpace - blankSpace;
				$(this).find('.mask-title').css('margin-right', maskTitleOffset + 'px');

				$(this).find('.mask-title').css('top', $currentSlide.find('.slide-title').offset().top + 'px');
			}
		});
	}
}

function goToSlide(index, direction){
	var totalSlides = $("#main-slide > section").length;

	$("#main-slide > section").removeClass('current next prev');
	$("#main-slide > section").eq(index).addClass('current');
	$("#main-slide #slide-paging > li").removeClass('current');
	var currentSlideNo = $("#main-slide > section.current").attr('data-slide-no');
	$("#main-slide #slide-paging > li > a[data-slide-no=" + currentSlideNo + "]").parent().addClass('current');

	// Order slide.
	var $currentSlide = $("#main-slide > section.current");
	var currentSlide = $("#main-slide > section").index($currentSlide) + 1;
	var currentSlideNo = parseInt($("#main-slide > section.current").attr('data-slide-no'));
	var currentSlideIndex = $("#main-slide > section").index($currentSlide);

	var previousSlideNo = currentSlideNo - 1;
	var nextSlideNo = currentSlideNo + 1;

	previousSlideNo = previousSlideNo < 0 ? totalSlides - 1 : previousSlideNo;
	nextSlideNo = nextSlideNo >= totalSlides ? 0 : nextSlideNo;

	var $previousSlide = $('#main-slide > section[data-slide-no=' + previousSlideNo + ']');
	var $nextSlide = $('#main-slide > section[data-slide-no=' + nextSlideNo + ']');

	$previousSlide.addClass('prev');
	$nextSlide.addClass('next');

	$previousSlide.insertBefore($currentSlide);
	$nextSlide.insertAfter($currentSlide);

	// Update mobile extra slide
	var currentSlideNoForExtra = $nextSlide.attr('data-slide-no');
	var prevSlideNoForExtra = $currentSlide.attr('data-slide-no');
	var nextSlideNoForExtra = $previousSlide.attr('data-slide-no');

	$("#main-slide > .mobile-extra-slide > section").removeClass('current next prev');
	$('#main-slide > .mobile-extra-slide > section[data-slide-no=' + currentSlideNoForExtra + ']').addClass('current');
	$('#main-slide > .mobile-extra-slide > section[data-slide-no=' + prevSlideNoForExtra + ']').addClass('prev');
	$('#main-slide > .mobile-extra-slide > section[data-slide-no=' + nextSlideNoForExtra + ']').addClass('next');

	var $currentExtraSlide = $("#main-slide > .mobile-extra-slide > section.current");
	var $prevExtraSlide = $("#main-slide > .mobile-extra-slide > section.prev");
	var $nextExtraSlide = $("#main-slide > .mobile-extra-slide > section.next");

	$prevExtraSlide.insertBefore($currentExtraSlide);
	$nextExtraSlide.insertAfter($currentExtraSlide);

	updateSlidePosition(direction);
}