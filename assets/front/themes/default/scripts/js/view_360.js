var imageSet = "";
var kId = $('#kitchen-id').val();
var $slider = false;
var detailBtnPosition = [];
var originalImageWidth = 1280;

$('body').addClass('view-360-page');

$(function(){
	if($('#drag-screen-mask').length > 0){
		$('#drag-screen-mask').click(function(){
			$(this).fadeOut();
		});

		setTimeout(function(){
			$('#drag-screen-mask').click();
		}, 2500);
	}

	$('#customize-btn').click(function(){
		$(this).parents('#customize-panel').toggleClass('open');
		$('body').toggleClass('open-customize');

		if($('#customize-panel').hasClass('open')){
			$('#header-panel').addClass('undermask-mobile');
		}else{
			$('#header-panel').removeClass('undermask-mobile');
		}
	});

	var windowWidth = $(window).outerWidth();
    var windowHeight = $(window).outerHeight()

	if(windowWidth < 768){
        if(windowWidth > windowHeight){
            // Landscape
            $('#view-360-panel').css('padding-left', '30px');
            $('#view-360-panel').css('padding-right', '30px');
        }else{
            // Portrait
            $('#view-360-panel').css('padding-left', '0px');
            $('#view-360-panel').css('padding-right', '0px');
        }
    }

	$(window).resize(function(){
        var windowWidth = $(window).outerWidth();
        var windowHeight = $(window).outerHeight()

        setTimeout(function(){
            if(windowWidth < 768){
                if(windowWidth > windowHeight){
                    // Landscape
                    $('#view-360-panel').css('padding-left', '30px');
                    $('#view-360-panel').css('padding-right', '30px');
                }else{
                    // Portrait
                    $('#view-360-panel').css('padding-left', '0px');
                    $('#view-360-panel').css('padding-right', '0px');
                }
            }
        }, 200);
    });

	// Click on component item
	$('#customize-panel .component-list > li > a').click(function(){
		$(this).parents('.component-list').find('li').removeClass('active');
		$(this).parents('li').addClass('active');
	});

	$('#customize-confirm-btn').click(function(){
		getImageSet();
		$('#customize-btn').click();
		$('html, body').scrollTop(0);
	});

	// $slider = $( "#slider" ).slider({
	// 	min: 1,
	// 	max: 200,
	// 	value: 1,
	// 	slide: function( event, ui ) {
	// 		var value = ui.value - 1;

	// 		updateImage(value);
	// 		updateDetailBtn(value);
	// 	}
	// });

	// var num = 1;
	// setInterval(function(){
	// 	updateImage(num);
	// 	updateDetailBtn(num);

	// 	num++;

	// 	if(num > 200){
	// 		num = 0;
	// 	}
	// }, 1/60);

	// Trigger the current active one after loading
	getImageSet();

	$('#body-wrapper').animateCss('fadeIn', 'in');

	$("#image-mask").swipe( { swipeStatus: swipe } );

	$('#detail-dialog').on('hidden.bs.modal', function(e){
		if($(this).find('iframe').length > 0){
			var iframe = $(this).find('iframe')[0];
			var iframeSrc = iframe.src;
			iframe.src = iframeSrc;
		}
	});

	$('.component-list-panel .component-list > li').matchHeight();

	$(document).on('click', '.btn-more-detail', function(){
		var detailId = $(this).attr('data-id');
		var $detailContent = $('#detail-playground .content-panel[data-id="' + detailId + '"]');
		var detailHtml = $detailContent.html();

		$('#detail-dialog').find('.content-panel').empty();
		$('#detail-dialog').find('.content-panel').append(detailHtml);

		var owl = $('#detail-dialog .owl-carousel').owlCarousel({
			margin: 0,
            padding: 0,
            items: 1,
            nav: true,
            dots: true,
            navSpeed : 500,
            dotSpeed : 500,
            navText: ['<div title="Previous" class="slide-arrow prev"></div>','<div title="Next" class="slide-arrow next"></div>'],
            autoplayHoverPause: true,
		})

		owl.on('changed.owl.carousel', function(event) {
		    if($('#detail-dialog video').length > 0){
		    	var promise = $('#detail-dialog video')[0].pause();

				if (promise !== undefined) {
				    promise.catch(error => {
				        // Auto-play was prevented
				        // Show a UI element to let the user manually start playback
				    }).then(() => {
				        // Auto-play started
				    });
				}
		    }
		});

		$('#detail-dialog').on('hidden.bs.modal', function(){
			if($('#detail-dialog video').length > 0){
				var promise = $('#detail-dialog video')[0].pause();

				if (promise !== undefined) {
				    promise.catch(error => {
				        // Auto-play was prevented
				        // Show a UI element to let the user manually start playback
				    }).then(() => {
				        // Auto-play started
				    });
				}
			}
		});

		$('#detail-dialog').modal();
	});

	const ps = new PerfectScrollbar('#customize-panel .component-list-container', {
		scrollXMarginOffset: 20
	});

	$(window).resize(function(){
		var imageIndex = $("#images-playground > img.current").index();
		updateDetailBtn(imageIndex);
	});
});

function swipe(event, phase, direction, distance) {
	var totalImages = $("#images-playground > img").length;
	var currentImageIndex = $("#images-playground > img.current").index();
	var offset = Math.round(distance / 40);

	// console.log("currentImageIndex", currentImageIndex);

    if(direction == 'right'){
    	var imageIndex = Math.max(currentImageIndex - offset, 0);
    	updateImage(imageIndex);
    	updateDetailBtn(imageIndex);

    	$("#swipe-text").text( phase +" you have swiped " + distance + "px in direction:" + direction );
    }else if(direction == 'left'){
    	var imageIndex = Math.min(currentImageIndex + offset, totalImages - 1);
    	updateImage(imageIndex);
    	updateDetailBtn(imageIndex);

    	$("#swipe-text").text( phase +" you have swiped " + distance + "px in direction:" + direction );
    }
}

function updateImage(imageIndex){
	$("#images-playground > img").removeClass('current');
	var $image = $("#images-playground > img").eq(imageIndex);
	$image.addClass('current');
	var imageUrl = $image.attr('src');

	console.log(imageUrl);

	$("#image").attr('src', imageUrl);
}

function updateDetailBtn(imageIndex){
	$('.btn-more-detail').each(function(){
		var id = $(this).attr('data-id');

		if(id in detailBtnPosition){
			var position = detailBtnPosition[id];
			var pageIndex = imageIndex;

			if(pageIndex < position.length){
				var position = position[pageIndex];
				var posX = position[0];
				var posY = position[1];

				// Calculate position according image size.
				var imageWidth = $("#image").width();
				var ratio = imageWidth / originalImageWidth;
				var newPosX = posX * ratio;
				var newPosY = posY * ratio;

				console.log("Original Image Width", originalImageWidth);
				console.log("Image Width", imageWidth);
				console.log("Ratio", ratio);
				console.log("X", newPosX);
				console.log("Y", newPosY);

				$(this).css('top', newPosY + 'px');
				$(this).css('left', newPosX + 'px');
			}
		}else{
			console.log("Image index [" + id + "] is not in Array.");
		}
	});
}

function getImageSet(){
	showLoadingPanel();

	var selectedComponentIds = [];

	$('#customize-panel .component-list').each(function(){
		var iId = $(this).find('li.active').attr('data-id');

		selectedComponentIds.push(iId);
	});

	var url = site_url + "Model/get_360_view_images";
	var componentItemId1 = selectedComponentIds[0];
	var componentItemId2 = selectedComponentIds[1];

	$.ajax({
		url: url,
		method: "POST",
		data: {
			kitchen_id: kId,
			component_item_id_1: componentItemId1,
			component_item_id_2: componentItemId2,
		},
		dataType: "json"
	}).done(function(response){
		var data = response.data;
		var totalImages = response.data.length;
		var loadedCount = 0;
		var imageSetId = response.set_id;

		// console.log("Total Images", totalImages);

		$("#images-playground").html('');

		for(var i = 0; i < totalImages; i++){
			var imageName = data[i]['image'];
			var imageUrl = uploads_url + "360-images/" + imageSetId + "/" + imageName;

			var img = new Image;
			img.onload = function(e) {
				loadedCount++;

				originalImageWidth = e.currentTarget.width;

				if(loadedCount == totalImages){
					if(totalImages > 0){
						// $slider.slider("option", "max", totalImages);
						// $slider.slider("value", 1);

						updateImage(0);
						updateDetailBtn(0);

						$("#image").css('opacity', 1);
					}

					hideLoadingPanel();
				}
			};

			img.onerror = function(e) {
				loadedCount++;

				if(loadedCount == totalImages){
					if(totalImages > 0){
						// $slider.slider("option", "max", totalImages);
						// $slider.slider("value", 1);

						updateImage(0);
						updateDetailBtn(0);

						$("#image").css('opacity', 1);
					}

					hideLoadingPanel();
				}
			};

			img.src = imageUrl;

			$("#images-playground").append('<img src="' + imageUrl + '">');
		}

		// Populate set detail button
		var setDetails = response.set_details;
		var detailBtnHtml = '';

		for(var i = 0; i < setDetails.length; i++){
			var detailId = setDetails[i]['id'];
			detailBtnHtml += '<a class="btn-more-detail" data-id="' + detailId + '"></a>';

			// Build template
			var title = setDetails[i]['title'];
			var subtitle = setDetails[i]['subtitle'];
			var description = setDetails[i]['description'];
			var mediaFiles = setDetails[i]['file'];
			var mediaFilesHtml = "";

			for(var j = 0; j < mediaFiles.length; j++){
				var file = mediaFiles[j]['file'];
				var isVideo = mediaFiles[j]['is_video'] == 1;
				file = uploads_url + 'detail-media/' + file;

				if(isVideo){
					// mediaFilesHtml += '<div class="item"><iframe width="100%" height="450" src="' + file + '" frameborder="0" encrypted-media" allowfullscreen></iframe></div>';
					mediaFilesHtml += '<div class="item"><video width="100%" height="450px" controls src="' + file + '"></video></div>';
				}else{
					mediaFilesHtml += '<div class="item" style="background-image: url(' + file + '"></div>';
				}
			}

			mediaFilesHtml = '<div class="owl-carousel owl-theme">' + mediaFilesHtml + '</div>';

			var detailDialogHtml = '<div class="content-panel" data-id="' + detailId + '"> \
										<div class="row no-gap"> \
											<div class="col-sm-7"> \
												<div class="media-col">' + mediaFilesHtml + '</div> \
											</div> \
											<div class="col-sm-5"> \
												<div class="content-col"> \
														<div class="title">' + title + '</div> \
														<div class="subtitle">' + subtitle + '</div> \
														<div class="description">' + description + '</div> \
													</div> \
												</div> \
											</div> \
										</div> \
									</div>';

			$('#detail-playground').append(detailDialogHtml);
		}

		$(".btn-more-detail").remove();
		$("#view-360-panel .image-wrapper").prepend(detailBtnHtml);

		// Set detail button position according to current image
		var detailBtnPositionData = response.set_details_position;
		// Clear old data
		detailBtnPosition = [];

		for(var i = 0; i < detailBtnPositionData.length; i++){
			var detailBtnInfo = detailBtnPositionData[i];

			for(var j = 0; j < detailBtnInfo.length; j++){
				var detailId = detailBtnInfo[j]['component_set_media_detail_id'];
				var posX = detailBtnInfo[j]['pos_x'];
				var posY = detailBtnInfo[j]['pos_y'];

				// If key exists
				if(!(detailId in detailBtnPosition)){
					detailBtnPosition[detailId] = new Array();
				}

				(detailBtnPosition[detailId]).push([posX, posY]);
			}
		}
	});
}

function pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}