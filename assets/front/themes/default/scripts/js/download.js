$('body').addClass('download-page');

$(function(){
	$('#subscription-form').submit(function(){
		showLoadingPanel();

		if(isFieldCompleted("#subscription-form")){
			var currentUrl = window.location.href;
			var options = {
	            success: subscriptionFormResponse,
	            data: { current_url : currentUrl },
	            dataType: 'json'
	        };

	        $(this).ajaxSubmit(options);
		}else{
			hideLoadingPanel();
		}

        return false;
	});
});

function subscriptionFormResponse(response, statusText, xhr, $form){
    if(response.result == false){
        var message = response.message;

        $('#message-dialog').on('hidden.bs.modal', function(){
        	$('[name="' + response.field_name + '"]').focus();
        });

        $('#message-dialog .modal-title').html('Message');
        $('#message-dialog .modal-body').html(message);

        hideLoadingPanel();

    	// Display message dialog
        $('#message-dialog').modal();
    }else{
    	$('#subscription-form input').val('');

    	hideLoadingPanel();

    	var message = "ระบบทำการบันทึกข้อมูลเรียบร้อยแล้ว";

    	$('#message-dialog .modal-body').addClass('text-center');
		$('#message-dialog .modal-footer').addClass('text-center');
        $('#message-dialog .modal-body').html(message);

        $('#message-dialog').modal();
    	// var redirectLink = response.redirect_link;

        // window.location.href = response.redirect_link;
    }
}