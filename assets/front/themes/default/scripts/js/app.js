$(function(){
	initAnimateCss();

	$(window).on('load', function(){
		if(!$('body').hasClass('home-page')){
			$('#body-wrapper').animateCss('fadeIn', 'in');
		}
	});

	initMobileHeaderPanel();

	// window.addEventListener('orientationchange', doOnOrientationChange);

	$(window).resize(function(){
		doOnOrientationChange();
	});

	// Initial execution if needed
	doOnOrientationChange();
});

function doOnOrientationChange() {
	var windowWidth = $(window).outerWidth();
	var windowHeight = $(window).outerHeight()

	setTimeout(function(){
		if(windowWidth < 768){
			if(windowWidth > windowHeight){
				// Portrait
			    // showLandscapeMask();
			}else{
				// Landscape
				hideLandscapeMask();
			}
		}else{
			hideLandscapeMask();
		}
	}, 200);

    // switch(screen.orientation.angle) {
    //  	case -90:
    //   	case 90:
    //     	showLandscapeMask();
    //     	break;
    //   	default:
	   //      hideLandscapeMask();
	   //      break;
    // }
}

function showLandscapeMask(){
	$('#landscape-mask').show();
}

function hideLandscapeMask(){
	$('#landscape-mask').hide();
}

function initMobileHeaderPanel(){
	$('.navbar-toggle').click(function(){
		$('#header-panel').toggleClass('open');
		$('#header-panel .menu').toggleClass('open');
		$('body').toggleClass('open-menu');
	});
}

function initAnimateCss(){
	$.fn.extend({
	    animateCss: function (animationName, type, callback) {
	    	if(type == 'in'){
            	$(this).removeClass('hide');
            	$(this).css('opacity', '1');
            }

	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
	            $(this).removeClass('animated ' + animationName);

	            if(type == 'out'){
	            	$(this).css('opacity', '0');
	            	$(this).addClass('hide');
	            }

	            if(typeof callback === 'function') {
                    callback();
                }
	        });
	    }
	});
}

function showLoadingPanel(){
    $('#loading-panel').show();
}

function hideLoadingPanel(){
    $('#loading-panel').hide();
}

function isFieldCompleted(panelId){
	var $currentStepPanel = $(panelId);
	var isFieldCompleted = true;

	$currentStepPanel.find('.error').removeClass('error');

	$currentStepPanel.find('.required').each(function(){
        // If it is textbox, textarea.
        if($(this).is('input, textarea')){
            if($(this).val() == ''){
                isFieldCompleted = false;
                $(this).addClass('error');
            }
        }
    });

    if(!isFieldCompleted){
        $currentStepPanel.find('.error').eq(0).focus();
        $currentStepPanel.find('.error').animateCss('pulse', 'in');
    }

    return isFieldCompleted;
}