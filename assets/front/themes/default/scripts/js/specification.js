$(function(){
	$('.spec-menu-item').click(function(){
		var itemId = $(this).attr('data-item-id');
		var targetOffset = $(".spec-item[data-item-id=" + itemId + "]").offset().top - 20;

		$("html, body").animate({
			scrollTop: targetOffset
		}, 500, 'swing', function() {
		});
	});

	const ps = new PerfectScrollbar('#spec-dropdown-menu');
});