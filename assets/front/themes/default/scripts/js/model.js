$(function(){
    var autoWidth = $(window).width() < 768 ? true : false;
    var slideMargin = $(window).width() < 768 ? 12 : 0;

    $('.gallery-image-list.owl-carousel').owlCarousel({
		loop: false,
        margin: slideMargin,
        stagePadding: 0,
        nav: false,
        dots: false,
        navsSpeed : 1000,
        dotsSpeed : 1000,
        navText: '',
        autoWidth: autoWidth,
        responsive:{
            0:{
                items: 2,
            },
            768:{
                items: 3,
            },
            1000:{
                items: 4,
            },
            1200:{
                items: 5,
            }
        }
	});

    if($(window).width() < 768){
        setInterval(function(){
            if($('.detail-mask-panel:visible').length > 0){
                var totalImages = $('.detail-mask-panel:visible .gallery-image-list.owl-carousel .owl-item').length;
                var index = $('.gallery-image-list.owl-carousel .item.active').parent().index();

                if($('body').hasClass('open-backdrop') || $('body').hasClass('modal-open')){
                    return;
                }

                if(index >= totalImages - 1){
                    index = 0;
                }else{
                    index++;
                }

                console.log("Image Index", index);

                $('.gallery-image-list.owl-carousel .item').eq(index).find('a').click();
            }
        }, 3000);
    }

    $('.gallery-image-list').each(function(){
    	$(this).find('li > a').click(function(e){
    		$(this).parents('.gallery-image-list').find('li').removeClass('active');
    		$(this).parent('li').addClass('active');

    		var imageUrl = $(this).css('background-image');
    		$(this).parents('.detail-mask-panel').find('.gallery-image-container').css('background-image', imageUrl);

            e.preventDefault();
            return false;
    	});

    	$(this).find('li.active > a').click();
    });

    $('.expand-text-btn').click(function(){
        $(this).parents('.mobile-model-content').toggleClass('open');
        $('body').toggleClass('open-backdrop');
    });

    $('#image-dialog').on('show.bs.modal', function(){
        var height = $(window).innerHeight();
        var windowWidth = $(window).outerWidth();
        var windowHeight = $(window).outerHeight()

        setTimeout(function(){
            if(windowWidth < 768){
                if(windowWidth > windowHeight){
                    // Landscape
                    $('#image-dialog .modal-content .item .img-wrapper').css('height', height);
                }else{
                    // Portrait
                    $('#image-dialog .modal-content .item .img-wrapper').css('height', 'auto');
                }

                $('#image-dialog .modal-content').css('height', height);
                $('#image-dialog .modal-content .item').css('height', height);
            }
        }, 200);
    });

    $(window).resize(function(){
        var height = $(window).innerHeight();

        var windowWidth = $(window).outerWidth();
        var windowHeight = $(window).outerHeight()

        setTimeout(function(){
            if(windowWidth < 768){
                if(windowWidth > windowHeight){
                    // Landscape
                    $('#image-dialog .modal-content .item .img-wrapper').css('height', height);
                }else{
                    // Portrait
                    $('#image-dialog .modal-content .item .img-wrapper').css('height', 'auto');
                }

                $('#image-dialog .modal-content').css('height', height);
                $('#image-dialog .modal-content .item').css('height', height);
            }
        }, 200);
    });

    $('#image-dialog .owl-carousel').owlCarousel({
            loop: false,
            margin: 0,
            padding: 0,
            items: 1,
            nav: true,
            dots: true,
            navSpeed : 1000,
            dotsSpeed : 1000,
            dragEndSpeed: 700,
            animateOut: 'fadeOut',
            navText: ['<div title="Previous" class="slide-arrow prev"></div>','<div title="Next" class="slide-arrow next"></div>'],
            autoplayHoverPause: true,
        })

    $('.mobile-model-content .fullscreen-btn').click(function(){
        $('#image-dialog').modal();
    });
});