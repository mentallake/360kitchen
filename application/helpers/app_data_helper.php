<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_item_status_text($status){
	$text = '';

	switch($status){
		case ITEM_STATUS_ACTIVE:
			$text = 'ใช้งาน';
			break;
		case ITEM_STATUS_INACTIVE:
			$text = 'ไม่ใช้งาน';
			break;
	}

	return $text;
}

function upload_file($image_name, $upload_config = FALSE){
    $current_datetime = new DateTime();
    $upload_date_time = $current_datetime->format('Y-m-d_H-i-s');

    $CI = & get_instance();  //get instance, access the CI superobject
    $CI->load->library('upload');

    $response = array(
        "result" => true,
        "image_name" => '',
        "message" => ''
        );

    // Set initiail value for upload library
    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = '*';
    $config['max_width']  = '0';
    $config['max_height']  = '0';

    if(isset($upload_config['file_name_prefix'])){
        $config['file_name'] = $upload_config['file_name_prefix'] . $upload_date_time;
    }else{
        $config['file_name']  = 'file_' . $upload_date_time;
    }

    if($upload_config){
        $config = array_merge($config, $upload_config);
    }

    $CI->upload->initialize($config);

    if (!$CI->upload->do_upload($image_name)){
        $error = $CI->upload->display_errors();
        $message = 'Result file: ' . $error;

        $response['result'] = false;
        $response['message'] = $message;
    }else{
        $file_data = $CI->upload->data();
        $file_name = $file_data['file_name'];
        $response['file_data'] = $file_data;
        $response['file_name'] = $file_name;

        // Consider image orientation
        image_fix_orientation('./uploads/' . $file_name);
    }

    return $response;
}