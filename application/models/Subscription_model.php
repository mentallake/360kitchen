<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription_model extends App_Model {
	private $DB = "subscriber";

	public function get_all_items(){
		$query = $this->db->get($this->DB);

		return $query->result_array();
	}

	public function update($data, $id){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->trans_start();

		if($id){
			$this->db->where('id', $id);
			$this->db->update($this->DB, $data);
		}else{
			$data['created_time'] = $current_datetime;

			$this->db->insert($this->DB, $data);
			$id = $this->db->insert_id();
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			log_message("ERROR", "[360 KITCHEN] TRANSACTION ROLLBALCK in Subscription_model::update");

		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}

		return $id;
	}
}