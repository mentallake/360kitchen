<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kitchen_model extends App_Model {
	private $DB = "kitchen";

	public function get_all_items($filters = array(), &$total_amount = false){
		$sql = "SELECT *
				FROM $this->DB k1
				WHERE deleted_time IS NULL";

		$default_order_by = " ORDER BY k1.id ASC";

		if(isset($filters['status'])){
			$status = $filters['status'];
			$sql .= " AND status = $status";
		}

		if(isset($filters['search'])){
			$search = $filters['search'];

			$sql .= " AND (k1.title LIKE '%$search%' OR
						   k1.code_name LIKE '%$search%')";
		}

		if(isset($filters['order_by'])){
			$order_by = $filters['order_by'];
			$order_dir = isset($filters['order_dir']) ? $filters['order_dir'] : "ASC";
			$order_by_parts = explode(',', $order_by);
			$order_text = "";

			for($i = 0; $i < count($order_by_parts); $i++){
				$order_text .= $order_text != "" ? "," : "";
				$order_text .= $order_by_parts[$i] . ' ' . $order_dir;
			}

			if($order_text != ""){
				$sql .= " ORDER BY $order_text";
			}else{
				$sql .= " ORDER BY k1.id ASC";
			}
		}else{
			$sql .= $default_order_by;
		}

		$query = $this->db->query($sql);
		$total_amount = $query->num_rows();

		$query_again = false;

		if(isset($filters['limit'])){
			$limit = $filters['limit'];
			$sql .= " LIMIT $limit";
			$query_again = true;
		}

		if(isset($filters['offset'])){
			$offset = $filters['offset'];
			$sql .= " OFFSET $offset";
			$query_again = true;
		}

		if($query_again){
			$query = $this->db->query($sql);
		}

		$result = array();
		$limit = false;

		if(isset($filters['limit'])){
			$limit = $filters['limit'];
		}

		$count = 0;

		foreach($query->result_array() as $row){
			$s_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_item_status_text($status);

			$result[] = $row;

			if($limit && $count >= $limit){
				break;
			}
		}

		return $result;
	}

	public function get_item($id){
		$this->db->where('kitchen.deleted_time IS NULL');
		$this->db->where('kitchen.id', $id);
		$query = $this->db->get('kitchen');

		$result = array();

		if($query->num_rows() > 0){
			$kitchen = $query->row_array();

			$this->db->order_by('image_order', 'asc');
			$this->db->where('kitchen_id', $id);
			$kg_query = $this->db->get('kitchen_gallery');

			$kitchen['gallery'] = $kg_query->result_array();

			$result = $kitchen;
		}

		return $result;
	}

	public function total_items(){
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		return $query->num_rows();
	}

	public function update($data, $id = false){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->trans_start();

		if($id){
			$data['updated_time'] = $current_datetime;

			$this->db->where('id', $id);
			$this->db->update($this->DB, $data);
		}else{
			$data['created_time'] = $current_datetime;

			$this->db->insert($this->DB, $data);
			$id = $this->db->insert_id();
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			log_message("ERROR", "[360 KITCHEN] TRANSACTION ROLLBALCK in Kitchen_model::update");

		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}

		return $id;
	}

	public function update_kitchen_image($data, $id){
		if($id){
			$this->db->where('id', $id);
			$this->db->update("kitchen_gallery", $data);
		}else{
			$this->db->insert("kitchen_gallery", $data);
			$id = $this->db->insert_id();
		}
	}

	public function actual_delete_kitchen_image($data){
        $ids = $data['ids'];

        $this->db->where_in('id', $ids);
        $this->db->delete('kitchen_gallery');
    }

    public function get_components($kitchen_id){
    	$this->db->where('kitchen_id', $kitchen_id);
    	$query = $this->db->get('kitchen_component');

    	$result = array();

    	foreach($query->result_array() as $row){
    		$c_id = $row['id'];

    		$this->db->where('component_id', $c_id);
    		$ci_query = $this->db->get('component_item');

    		$row['items'] = $ci_query->result_array();

    		$result[] = $row;
    	}

    	return $result;
    }

    public function get_360_view_images($component_item_id_1, $component_item_id_2){
    	$this->db->where('component_item_id_1', $component_item_id_1);
    	$this->db->where('component_item_id_2', $component_item_id_2);
    	$query = $this->db->get('component_set');

    	$result = array();

    	if($query->num_rows() > 0){
    		$info = $query->row_array();
    		$set_id = $info['id'];

    		$this->db->where('component_set_id', $set_id);
    		$s_query = $this->db->get('component_set_media');

    		$result = $s_query->result_array();
    	}

    	return $result;
    }

    public function get_set_details($set_id){
    	$this->db->where('component_set_id', $set_id);
    	$query = $this->db->get('component_set_media_detail');

    	$result = array();

    	foreach($query->result_array() as $row){
    		$r_id = $row['id'];

    		$this->db->where('component_media_detail_id', $r_id);
    		$c_query = $this->db->get('component_media_detail_file');

    		$row['file'] = $c_query->result_array();

    		$result[] = $row;
    	}

    	return $result;
    }

    public function get_set_details_by_kitchen_id($kitchen_id){
    	$this->db->where('kitchen_id', $kitchen_id);
    	$query = $this->db->get('component_set_media_detail');

    	$result = array();

    	foreach($query->result_array() as $row){
    		$r_id = $row['id'];

    		$this->db->where('component_media_detail_id', $r_id);
    		$c_query = $this->db->get('component_media_detail_file');

    		$row['file'] = $c_query->result_array();

    		$result[] = $row;
    	}

    	return $result;
    }

    public function get_set_details_position($set_detail_id){
    	$this->db->where('component_set_media_detail_id', $set_detail_id);
    	$query = $this->db->get('component_set_image_detail_map');

    	return $query->result_array();
    }

    public function get_set_details_position_by_kitchen_id($kitchen_id){
    	$this->db->where('kitchen_id', $kitchen_id);
    	$query = $this->db->get('component_set_image_detail_map');

    	return $query->result_array();
    }

	public function get_new_model(){
		$item = array(
			"id" => false,
			"code_name" => "",
			"title" => "",
			"description" => "",
			"image" => "",
			"detail_description" => "",
			"status" => ITEM_STATUS_INACTIVE,
			"status_text" => get_item_status_text(ITEM_STATUS_INACTIVE),
			"created_time" => "",
			"updated_time" => NULL,
			"deleted_time" => NULL,
			"gallery" => array()
		);

		return $item;
	}
}