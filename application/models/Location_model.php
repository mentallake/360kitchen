<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Location_model extends App_Model {
	public function get_all_locations(){
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get('location');

		return $query->result_array();
	}
}