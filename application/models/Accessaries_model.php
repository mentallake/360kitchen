<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Accessaries_model extends App_Model {
	private $DB = "accessaries";

	public function get_all_items($filters = array(), &$total_amount = false){
		$sql = "SELECT *
				FROM $this->DB s1
				WHERE deleted_time IS NULL";

		$default_order_by = " ORDER BY s1.name ASC";

		if(isset($filters['status'])){
			$status = $filters['status'];
			$sql .= " AND status = $status";
		}

		if(isset($filters['search'])){
			$search = $filters['search'];

			$sql .= " AND (s1.name LIKE '%$search%')";
		}

		if(isset($filters['order_by'])){
			$order_by = $filters['order_by'];
			$order_dir = isset($filters['order_dir']) ? $filters['order_dir'] : "ASC";
			$order_by_parts = explode(',', $order_by);
			$order_text = "";

			for($i = 0; $i < count($order_by_parts); $i++){
				$order_text .= $order_text != "" ? "," : "";
				$order_text .= $order_by_parts[$i] . ' ' . $order_dir;
			}

			if($order_text != ""){
				$sql .= " ORDER BY $order_text";
			}else{
				$sql .= " ORDER BY s1.id ASC";
			}
		}else{
			$sql .= $default_order_by;
		}

		$query = $this->db->query($sql);
		$total_amount = $query->num_rows();

		$query_again = false;

		if(isset($filters['limit'])){
			$limit = $filters['limit'];
			$sql .= " LIMIT $limit";
			$query_again = true;
		}

		if(isset($filters['offset'])){
			$offset = $filters['offset'];
			$sql .= " OFFSET $offset";
			$query_again = true;
		}

		if($query_again){
			$query = $this->db->query($sql);
		}

		$result = array();
		$limit = false;

		if(isset($filters['limit'])){
			$limit = $filters['limit'];
		}

		$count = 0;

		foreach($query->result_array() as $row){
			$s_id = $row['id'];
			$status = $row['status'];
			$row['status_text'] = get_item_status_text($status);

			$result[] = $row;

			if($limit && $count >= $limit){
				break;
			}
		}

		return $result;
	}

	public function get_item($id){
		$this->db->where('id', $id);
		$query = $this->db->get($this->DB);

		return $query->row_array();
	}

	public function total_items(){
		$this->db->where('deleted_time IS NULL');
		$query = $this->db->get($this->DB);

		return $query->num_rows();
	}

	public function update($data, $id = false){
		$current_datetime = date("Y-m-d H:i:s");

		$this->db->trans_start();

		if($id){
			$data['updated_time'] = $current_datetime;

			$this->db->where('id', $id);
			$this->db->update($this->DB, $data);
		}else{
			$data['created_time'] = $current_datetime;

			$this->db->insert($this->DB, $data);
			$id = $this->db->insert_id();
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			log_message("ERROR", "[360 KITCHEN] TRANSACTION ROLLBALCK in Accessaries_model::update");

		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}

		return $id;
	}

	public function get_new_model(){
		$item = array(
			"id" => false,
			"name" => "",
			"description" => "",
			"image" => "",
			"status" => ITEM_STATUS_INACTIVE,
			"status_text" => get_item_status_text(ITEM_STATUS_INACTIVE),
			"created_time" => "",
			"updated_time" => NULL,
			"deleted_time" => NULL,
		);

		return $item;
	}
}