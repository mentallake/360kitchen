<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function view($model_name){
		$title = "Model";
		$model_id = false;

		$this->load->model('kitchen_model');

		switch(strtolower($model_name)){
			case "urbano":
				$title = "Urbano";
				$submenu = MENU_MODEL_URBANO;
				$model_id = 1;
				break;
			case "tadao":
				$title = "Tadao";
				$submenu = MENU_MODEL_TADAO;
				$model_id = 2;
				break;
			case "tuscano":
				$title = "Tuscano";
				$submenu = MENU_MODEL_TUSCANO;
				$model_id = 3;
				break;
		}

		$kitchen = $this->kitchen_model->get_item($model_id);
		$view_360_url = site_url('Model/view_360/' . $kitchen['code_name']);

		$data = array(
			"menu" => MENU_MODEL,
			"submenu" => $submenu,
			"title" => $title,
			"kitchen" => $kitchen,
			"view_360_url" => $view_360_url
		);

		$this->add_data($data);
		$this->load->library('template');

		$this->template->add_css(assets_libs_url('OwlCarousel2-2.2.1/assets/owl.carousel.min.css'));
		$this->template->add_css(assets_libs_url('OwlCarousel2-2.2.1/assets/owl.theme.default.min.css'));
		$this->template->add_js(assets_libs_url('OwlCarousel2-2.2.1/owl.carousel.min.js'));
		$this->template->add_js(assets_scripts_url('js/model.js'));
		$this->template->load($this->template_name, 'model_view', $this->data);
	}

	public function view_360($model_name){
		$title = "Model";
		$page_bg = "";
		$model_id = false;
		$prev_model_url = "";
		$prev_model_name = "";
		$next_model_url = "";
		$next_model_name = "";

		$this->load->model('kitchen_model');

		switch(strtolower($model_name)){
			case "urbano":
				$title = "Urbano";
				$submenu = MENU_MODEL_URBANO;
				$model_id = 1;
				$page_bg = assets_images_url('360-urbano-bg.jpg');
				$prev_model_url = site_url('Model/view_360/Tuscano');
				$prev_model_name = "Tuscano";
				$next_model_url = site_url('Model/view_360/Tadao');
				$next_model_name = "Tadao";
				break;
			case "tadao":
				$title = "Tadao";
				$submenu = MENU_MODEL_TADAO;
				$model_id = 2;
				$page_bg = assets_images_url('360-tadao-bg.jpg');
				$prev_model_url = site_url('Model/view_360/Urbano');
				$prev_model_name = "Urbano";
				$next_model_url = site_url('Model/view_360/Tuscano');
				$next_model_name = "Tuscano";
				break;
			case "tuscano":
				$title = "Tuscano";
				$submenu = MENU_MODEL_TUSCANO;
				$model_id = 3;
				$page_bg = assets_images_url('360-tuscano-bg.jpg');
				$prev_model_url = site_url('Model/view_360/Tadao');
				$prev_model_name = "Tadao";
				$next_model_url = site_url('Model/view_360/Urbano');
				$next_model_name = "Urbano";
				break;
		}

		$kitchen = $this->kitchen_model->get_item($model_id);
		$components = $this->kitchen_model->get_components($model_id);

		$style_360 = true;
		$back_btn_url = site_url('Model/view/' . $model_name);

		$data = array(
			"menu" => MENU_VIEW_360,
			"submenu" => $submenu,
			"title" => $title,
			"kitchen_id" => $model_id,
			"kitchen" => $kitchen,
			"components" => $components,
			"page_bg" => $page_bg,
			"model_name" => $title,
			"style_360" => $style_360,
			"back_btn_url" => $back_btn_url,
			"prev_model_url" => $prev_model_url,
			"prev_model_name" => $prev_model_name,
			"next_model_url" => $next_model_url,
			"next_model_name" => $next_model_name,
		);

		$this->add_data($data);
		$this->load->library('template');

		$this->template->add_css(assets_libs_url('OwlCarousel2-2.2.1/assets/owl.carousel.min.css'));
		$this->template->add_css(assets_libs_url('OwlCarousel2-2.2.1/assets/owl.theme.default.min.css'));
		$this->template->add_js(assets_libs_url('OwlCarousel2-2.2.1/owl.carousel.min.js'));
		$this->template->add_js(assets_scripts_url('js/view_360.js'));
		$this->template->load($this->template_name, 'view_360_view', $this->data);
	}

	public function get_360_view_images(){
		$kitchen_id = $this->input->post('kitchen_id');
		$component_item_id_1 = $this->input->post('component_item_id_1');
		$component_item_id_2 = $this->input->post('component_item_id_2');

		$this->load->model('kitchen_model');
		$images_info = $this->kitchen_model->get_360_view_images($component_item_id_1, $component_item_id_2);
		$set_details = array();
		$set_details_position = array();
		$set_id = false;

		if(count($images_info) > 0){
			$set_id = $images_info[0]['component_set_id'];
			$set_details = $this->kitchen_model->get_set_details_by_kitchen_id($kitchen_id);

			for($i = 0; $i < count($set_details); $i++){
				$set_detail = $set_details[$i];
				$sd_id = $set_detail['id'];

				$set_details_position[] = $this->kitchen_model->get_set_details_position_by_kitchen_id($kitchen_id);
			}
		}

		$result = true;
		$message = "";

		$response = array(
			"result" => $result,
			"message" => $message,
			"data" => $images_info,
			"set_id" => $set_id,
			"set_details" => $set_details,
			"set_details_position" => $set_details_position
		);

		echo json_encode($response);
	}
}
