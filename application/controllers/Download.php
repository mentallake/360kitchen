<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function index(){
		$this->load->model('download_model');

		$filters = array(
			"status" => ITEM_STATUS_ACTIVE
		);

		$download_items = $this->download_model->get_all_items($filters);

		$data = array(
			"title" => "Download",
			"menu" => MENU_DOWNLOAD,
			"download_items" => $download_items
		);

		$this->add_data($data);
		$this->load->library('template');

		$this->template->add_js(assets_libs_url('jquery-form.min.js'));
		$this->template->add_js(assets_scripts_url('js/download.js'));
		$this->template->load($this->template_name, 'download_view', $this->data);
	}

	public function subscribe(){
		$email = $this->input->post('email');

		$data = array(
			"email" => $email
		);

		$this->load->model('subscription_model');
		$this->subscription_model->update($data, false);

		$result = true;
		$message = '';

		$response = array(
			"result" => $result,
			"message" => $message
			);

		echo json_encode($response);
	}
}
