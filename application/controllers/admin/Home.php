<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function index($show_error = false){
		if($this->is_admin_logged_in){
            redirect('admin/Kitchen');
        }else{
        	redirect('admin/Login');
        }
    }
}