<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function index($show_error = false){
		if($this->is_admin_logged_in){
            redirect('admin/Kitchen');
        }

		$this->load->helper('form');

		$data = array(
			"menu" => MENU_ADMIN_LOGIN,
			"page_title" => "เข้าสู่ระบบ",
			"show_error" => $show_error
		);

		$this->load->view('admin/themes/default/login_view', $data);
	}

	public function perform_login() {
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		$this->db->where("username", $username);
		$query = $this->db->get("admin");

		if ($query->num_rows() > 0) {
			$user = $query->row_array();

			$password_hash = $user["password"];

			if (password_verify($password, $password_hash)) {
				$this->set_user_info_session($user['id']);
				redirect('admin/Kitchen');
				return;
			} else {
				redirect('admin/login/index/' . true);
				return;
			}
		} else {
			redirect('admin/Login/index/' . true);
			return;
		}
	}

	public function set_user_info_session($user_id){
		$this->load->database();

        $this->db->where("id", $user_id);
		$query = $this->db->get("admin");

		if($query->num_rows() > 0){
			$admin_info = $query->row_array();
			unset($admin_info['password']);
			$this->session->set_userdata("360_admin_info", $admin_info);
		}
	}

	public function test_password($password = "admin") {
		echo password_hash($password, PASSWORD_DEFAULT);
	}
}