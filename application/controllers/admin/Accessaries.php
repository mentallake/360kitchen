<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accessaries extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();

        $this->verify_if_admin_login();
    }

	public function index(){
		$this->load->helper('form');

		$data = array(
			"menu" => MENU_ADMIN_ACCESSARIES,
			"page_title" => "จัดการ Option Accessaries"
		);

		$this->add_data($data);
		$this->load->library('admin_template');

		$this->admin_template->add_js(assets_admin_scripts_url('js/table.js'));
		$this->admin_template->add_js(assets_admin_scripts_url('js/accessaries_table.js'));
		$this->admin_template->load($this->template_name, 'accessaries_view', $this->data);
	}

	public function edit($id = false){
		$this->load->helper('form');
		$this->load->model('accessaries_model');

		if($id){
			$item = $this->accessaries_model->get_item($id);
		}else{
			$item = $this->accessaries_model->get_new_model();
		}

		$status_options = array(
			ITEM_STATUS_INACTIVE => get_item_status_text(ITEM_STATUS_INACTIVE),
			ITEM_STATUS_ACTIVE => get_item_status_text(ITEM_STATUS_ACTIVE),
		);

		$title = $id ? "แก้ไขข้อมูล" : "เพิ่มข้อมูล Option Accessaries ใหม่";
		$page_title = ($id ? '<i class="fa fa-pencil"></i>&nbsp;&nbsp;' : '<i class="fa fa-plus"></i>&nbsp;&nbsp;') . $title;

		$data = array(
			"menu" => MENU_ADMIN_ACCESSARIES,
			"title" => $title,
			"page_title" => $page_title,
			"item" => $item,
			"status_options" => $status_options,
		);

		$this->add_data($data);
		$this->load->library('admin_template');
		$this->admin_template->add_js(assets_admin_scripts_url('js/editor.js'));
		$this->admin_template->add_js(assets_admin_scripts_url('js/edit_accessaries.js'));
		$this->admin_template->load($this->template_name, 'edit_accessaries_view', $this->data);
	}

	public function update(){
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$description = $this->input->post('description');
		$status = $this->input->post('status');
		$current_url = $this->input->post('current_url');

		$is_new_member = $id == false;
		$msg_title = $is_new_member ? "เพิ่มข้อมูล Accessaries" : "แก้ไขข้อมูล Accessaries";

		$data = array(
			"name" => $name,
			"description" => $description,
			"status" => $status,
		);

		$image_name = 'image';
		$is_image_uploaded = (!empty($_FILES[$image_name]) && $_FILES[$image_name]['name'] != '');

		$this->load->library('upload');

		if ($is_image_uploaded){
		    $config['upload_path'] = './uploads/';
		    $config['file_name_prefix'] = 'oa_image_';
		    $config['allowed_types'] = 'gif|jpg|png';
			$ui_response = upload_file($image_name, $config);

			if($ui_response['result'] && $ui_response['file_name'] != ''){
				$data['image'] = $ui_response['file_name'];
			}else{
				$response = array(
					"result" => false,
					"message" => $ui_response['message'],
					"redirect_link" => $current_url
					);

				echo json_encode($response);
				return;
			}
		}else{
			// If new data and no image right now, use the default one.
			if(!$id){
				// $data['image'] = 'default-image.png';
			}
		}

		// Update data
		$this->load->model('accessaries_model');
		$dl_id = $this->accessaries_model->update($data, $id);

		$redirect_link = site_url('admin/Accessaries/?title=' . $msg_title . '&msg=บันทึกสำเร็จ');

		$result = true;
		$message = '';

		$response = array(
			"result" => $result,
			"message" => $message,
			"redirect_link" => $redirect_link
			);

		echo json_encode($response);
	}

	public function delete($id){
		$this->load->model('accessaries_model');

		$result = true;
		$message = '';

		$current_datetime = date("Y-m-d H:i:s");

		$data = array(
			"deleted_time" => $current_datetime
		);

		$this->accessaries_model->update($data, $id);

		$response = array(
			"result" => $result,
			"message" => $message,
			"updated_id" => $id,
		);

		echo json_encode($response);
	}

	public function data_processing(){
		$draw = $this->input->post('draw');
		$item_start = $this->input->post('start');
		$item_per_page = $this->input->post('length');
		$order = $this->input->post('order');
		$search = $this->input->post('search');
		$status = $this->input->post('status');

		$draw = $draw ? $draw : 1;
		$item_start = $item_start ? $item_start : 0;
		$item_per_page = $item_per_page ? $item_per_page : 10;

		$search_text = $search['value'];
		$order_by = false;
		$order_dir = false;

		$columns = array(
			"checkbox",
			"id",
			"name",
			"status",
			"created_time",
			"updated_time"
		);

		if(count($order) > 0){
			$order_data = $order[0];
			$order_by_index = $order_data['column'];
			$order_dir = $order_data['dir'];
			$order_by = $columns[$order_by_index];
		}

		$filters = array(
			"limit" => $item_per_page,
			"offset" => $item_start
		);

		if($search_text != ''){
			$filters['search'] = $search_text;
		}

		if($order_by && $order_by != ''){
			$filters['order_by'] = $order_by;
		}

		if($order_dir && $order_dir != ''){
			$filters['order_dir'] = $order_dir;
		}

		if($status !== false && $status != ''){
			$filters['status'] = $status;
		}

		$this->load->model('accessaries_model');

		$total_items = $this->accessaries_model->total_items();
		$total_filter_items = 0;
		$items = $this->accessaries_model->get_all_items($filters, $total_filter_items);

		$table_data = array(
			"draw" => $draw,
			"recordsTotal" => $total_items,
  			"recordsFiltered" => $total_filter_items,
  			"order_by" => $order_by,
  			"order_dir" => $order_dir,
  			"search" => $search_text,
		);

		$data = array();

		for($i = 0; $i < count($items) && $i < $item_per_page; $i++){
			$data_row = array();

			$item_id = $items[$i]['id'];
			$data_row['id'] = $item_id;
			$data_row['no'] = (string)($item_start + $i + 1);
			$data_row['name'] = $items[$i]['name'];

			$status = $items[$i]['status'];
			$data_row['status_text'] = $items[$i]['status_text'];
			$status_class = $status ? 'status-active' : 'status-inactive';
			$data_row['status_class'] = $status_class;

			$data_row['created_time'] = $items[$i]['created_time'];
			$data_row['updated_time'] = $items[$i]['updated_time'] == NULL ? '-' : $items[$i]['updated_time'];

			$data[] = $data_row;
		}

		$table_data['data'] = $data;

		echo json_encode($table_data);
	}
}
