<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function index(){
		$this->load->model('location_model');
		$locations = $this->location_model->get_all_locations();

		$data = array(
			"title" => "Store Location",
			"menu" => MENU_LOCATION,
			"locations" => $locations
		);

		$this->add_data($data);
		$this->load->library('template');

		$this->template->add_js(assets_scripts_url('js/location.js'));
		$this->template->load($this->template_name, 'location_view', $this->data);
	}
}
