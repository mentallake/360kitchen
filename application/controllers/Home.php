<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function index(){
		$this->load->library('user_agent');
		$this->load->model('kitchen_model');
		$kitchens = $this->kitchen_model->get_all_items();

		for($i = 0; $i < count($kitchens); $i++){
			$k_id = $kitchens[$i]['id'];

			$k_info = $this->kitchen_model->get_item($k_id);

			$kitchens[$i]['gallery'] = $k_info['gallery'];
		}

		$data = array(
			"menu" => MENU_HOME,
			"title" => "Home",
			"kitchens" => $kitchens,
		);

		$this->add_data($data);
		$this->load->library('template');

		$this->template->add_js(assets_libs_url('jquery.horizonScroll.js'));
		$this->template->add_css(assets_libs_url('OwlCarousel2-2.2.1/assets/owl.carousel.min.css'));
		$this->template->add_css(assets_libs_url('OwlCarousel2-2.2.1/assets/owl.theme.default.min.css'));
		$this->template->add_js(assets_libs_url('OwlCarousel2-2.2.1/owl.carousel.min.js'));
		$this->template->add_js(assets_scripts_url('js/model.js'));
		$this->template->add_js(assets_scripts_url('js/home.js'));
		$this->template->load($this->template_name, 'home_view', $this->data);
	}
}
