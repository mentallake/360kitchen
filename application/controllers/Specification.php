<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Specification extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function standard(){
		$this->load->model('specification_model');
		$filters = array(
			"status" => ITEM_STATUS_ACTIVE
		);
		$items = $this->specification_model->get_all_items($filters);

		$data = array(
			"title" => "Standard Specification",
			"menu" => MENU_SPECIFICATION,
			"submenu" => MENU_SPECIFICATION_STANDARD,
			"items" => $items
		);

		$this->add_data($data);
		$this->load->library('template');

		$this->template->add_js(assets_scripts_url('js/specification.js'));
		$this->template->load($this->template_name, 'specification_view', $this->data);
	}

	public function option_accessaries(){
		$this->load->model('accessaries_model');
		$filters = array(
			"status" => ITEM_STATUS_ACTIVE
		);
		$items = $this->accessaries_model->get_all_items($filters);

		$data = array(
			"title" => "Option Accessaries",
			"menu" => MENU_SPECIFICATION,
			"submenu" => MENU_SPECIFICATION_OPTION,
			"items" => $items
		);

		$this->add_data($data);
		$this->load->library('template');

		$this->template->add_js(assets_scripts_url('js/specification.js'));
		$this->template->load($this->template_name, 'accessaries_view', $this->data);
	}
}
