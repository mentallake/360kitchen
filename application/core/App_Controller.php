<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Controller extends CI_Controller {
	var $ci;
    var $title = APP_NAME;
    var $menu = MENU_NONE;
    var $submenu = MENU_NONE;
    var $data = array();
    var $template_name = 'default';
    var $logged_in_user_info = false;
    var $is_user_logged_in = false;

    var $logged_in_admin_info = false;
    var $is_admin_logged_in = false;

    /**
	 * Constructor
	 */
    public function __construct(){
        parent::__construct();

        date_default_timezone_set('Asia/Bangkok');

        $this->ci =& get_instance();

        $logged_in_admin_info = $this->session->userdata('360_admin_info');

        if($logged_in_admin_info){
            $this->logged_in_admin_info = $logged_in_admin_info;
            $this->is_admin_logged_in = true;

            $this->update_admin_info_session();
        }

        $session_expiration = $this->config->item('sess_expiration');

        $data = array(
            "title" => $this->title,
            "menu" => $this->menu,
            "submenu" => $this->submenu,
            "admin_info" => $this->logged_in_admin_info,
            "is_admin_logged_in" => $this->is_admin_logged_in,
            "session_expiration" => $session_expiration,
        );

        $this->add_data($data);
    }

	function add_data($data){
        $data_keys = array_keys($data);
        $data_values = array_values($data);

        for($i = 0; $i < count($data_keys); $i++){
            $this->data[$data_keys[$i]] = $data_values[$i];
        }
    }

    public function do_login($user_info){
    }

    public function save_user_info_in_session($account, $remember_me = FALSE){
    }

    function update_admin_info_session(){
        $admin_info = $this->session->userdata('logged_in_admin_info');
        $admin_id = $admin_info['id'];

        $this->db->where("id", $admin_id);
        $query = $this->db->get("admin");

        if($query->num_rows() > 0){
            $admin_info = $query->row_array();
            unset($user_info['password']);
            $this->session->set_userdata("360_admin_info", $admin_info);
        }
    }

    function update_user_info_session(){
    }

    function do_logout(){
    }

    function perform_admin_logout(){
        $this->session->unset_userdata('360_admin_info');
        $this->is_admin_logged_in = false;

        redirect('admin/Login');
    }

    function verify_if_login(){
        if(!$this->is_user_logged_in){
            redirect('home');
        }
    }

    function verify_if_admin_login(){
        if(!$this->is_admin_logged_in){
            redirect('admin/Login');
        }
    }
}

/* End of file app_controller.php */
/* Location: ./application/core/app_controller.php */