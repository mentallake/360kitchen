<div id="model-page" class="detail-mask-panel">
	<div class="model-container">
		<div class="gallery-image-container">
			<div class="mobile-model-content-mask"></div>
			<div class="detail-text-mask"></div>

			<div class="mobile-model-content visible-xs">
				<div class="model-name">
					<?php echo $kitchen['code_name']; ?>
				</div>
				<div class="detail-content">
					<div class="detail-text">
						<?php echo $kitchen['detail_description']; ?>
					</div>
					<a class="expand-text-btn"></a><a class="fullscreen-btn">FULL GALLERY</a>
				</div>
				<div class="btn-360-wrapper">
					<a href="<?php echo $view_360_url; ?>" class="btn-360"></a>
				</div>
			</div>
		</div>

		<div id="kitchen-model-content" class="hidden-xs">
			<div class="container">
				<div class="model-name hidden-xs">
					<?php echo $kitchen['code_name']; ?>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="gallery-label-wrapper">
							<div class="label-text hidden-xs">GALLERY</div>
						</div>
						<ul class="gallery-image-list owl-carousel">
							<?php
							$images = $kitchen['gallery'];

							for($i = 0; $i < count($images); $i++){
								$image_url = uploads_url($images[$i]['image']);
							?>
							<li class="item <?php echo $i == 0 ? 'active' : ''; ?>">
								<a style="background-image: url(<?php echo $image_url; ?>);"></a>
							</li>
							<?php } ?>
						</ul>
					</div>
					<div class="col-sm-6 hidden-xs">
						<div class="detail-text">
							<?php echo $kitchen['detail_description']; ?>
						</div>
						<div class="btn-360-wrapper">
							<a href="<?php echo $view_360_url; ?>" class="btn-360 btn-red"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="image-dialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="container vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>

				<div class="model-name"><?php echo $kitchen['code_name']; ?></div>

				<div class="owl-carousel owl-theme">
					<?php
					$images = $kitchen['gallery'];

					for($i = 0; $i < count($images); $i++){
						$image_url = uploads_url($images[$i]['image']);
					?>
					<div class="item">
						<div class="img-wrapper">
							<img src="<?php echo $image_url; ?>" alt="">
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>