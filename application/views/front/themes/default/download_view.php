<div id="download-page">
	<div class="container">
		<div id="download-panel" class="content-page-panel type-1">
			<div class="content-panel-header">
				<div class="row">
					<div class="col-sm-8 col-md-9">
						<div class="content-panel-title text-white">Download</div>
					</div>
					<div class="col-sm-4 col-md-3">
						<form id="subscription-form" method="POST" action="<?php echo site_url('Download/subscribe'); ?>">
							<div class="remark eng text-white text-right"><span class="text-red-1">*</span>Please, Subscribe e-mail to get update</div>
							<div class="input-wrapper">
								<input type="email" id="email-textbox" name="email" class="form-control eng type-1 required" placeholder="Enter e-mail here">
								<button type="submit" id="subscribe-button" class="btn btn-circle">
									<i class="fa fa-angle-right"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="content-panel-body">
				<div class="row narrow">
					<?php
					for($i = 0; $i < count($download_items); $i++){
						$item = $download_items[$i];
						$file_url = uploads_url($item['filename']);
					?>
					<div class="download-item-col col-xs-6 col-sm-4 col-md-3">
						<a class="download-item animated fadeIn" href="<?php echo $file_url; ?>" target="_blank">
							<div class="download-image">
								<div class="mask hide">
									<div class="mask-content vertical-middle">
										<span class="text-1 text-uppercase">Please</span><br>
										Subscribe Newsletter<br>
										Before Download
									</div>
								</div>
								<img src="<?php echo uploads_url($item['image']); ?>" alt="">
							</div>
							<div class="download-content">
								<div class="download-title"><?php echo $item['title']; ?></div>
								<div class="download-issue">
									<span class="label-text">Issue Date: </span><span class="download-issue-date text-uppercase"><?php echo mysqldatetime_to_date($item['issue_date'], "M d, Y"); ?></span>
								</div>
							</div>
						</a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>