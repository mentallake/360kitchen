<div id="header-panel">
	<?php if($menu != MENU_HOME){ ?>
	<div class="container">
	<?php } ?>
		<div id="header-top-panel">
			<?php if(!isset($style_360) || !$style_360){ ?>
			<ul id="header-top-menus">
				<li>
					<a href="<?php echo site_url(); ?>"><img src="<?php echo assets_images_url('home-icon.png'); ?>" alt=""></a>
				</li><li>
					<a href="tel:022750029"><img src="<?php echo assets_images_url('call-icon.png'); ?>" alt=""></a>
				</li><li class="">
					<a href="https://www.facebook.com/360kitchen" target="_blank"><img src="<?php echo assets_images_url('fb-icon.png'); ?>" class="fb" alt=""></a>
				</li>
			</ul>
			<?php }else{ ?>
			<a href="<?php echo $back_btn_url; ?>" id="back-to-main-btn">
				<img src="<?php echo assets_images_url('back-half-arrow.png'); ?>" alt="">Back to main
			</a>

			<div class="model-name"><?php echo $model_name; ?></div>
			<?php } ?>
		</div>

		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header clearfix">
			<?php if(!isset($style_360) || !$style_360){ ?>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-menu" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<!-- <span class="icon-bar first"></span>
				<span class="icon-bar second"></span>
				<span class="icon-bar third"></span> -->
				<div class="menu menu-btn" data-menu="11">
			        <div class="icon-left"></div>
			        <div class="icon-right"></div>
			    </div>
			</button>
			<?php } ?>
			<a class="navbar-brand" href="<?php echo site_url(); ?>">
				<img class="slide-logo" src="<?php echo assets_images_url('logo.png'); ?>" alt="">
			</a>
		</div>

		<?php if(!isset($style_360) || !$style_360){ ?>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="collapse-menu">
			<ul id="" class="nav navbar-nav navbar-right">
				<li id="" class="dropdown <?php echo $menu == MENU_MODEL ? 'active' : ''; ?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Model</a>
					<ul class="dropdown-menu dropdown-menu-left">
						<li class="<?php echo $submenu == MENU_MODEL_URBANO ? 'active' : ''; ?>"><a href="<?php echo site_url('Model/view/urbano'); ?>">Urbano</a></li>
						<li class="<?php echo $submenu == MENU_MODEL_TADAO ? 'active' : ''; ?>"><a href="<?php echo site_url('Model/view/tadao'); ?>">Tadao</a></li>
						<li class="<?php echo $submenu == MENU_MODEL_TUSCANO ? 'active' : ''; ?>"><a href="<?php echo site_url('Model/view/tuscano'); ?>">Tuscano</a></li>
					</ul>
				</li>
				<li id="" class="dropdown <?php echo $menu == MENU_SPECIFICATION ? 'active' : ''; ?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Specification</a>
					<ul class="dropdown-menu dropdown-menu-left">
						<li class="<?php echo $submenu == MENU_SPECIFICATION_STANDARD ? 'active' : ''; ?>"><a href="<?php echo site_url('Specification/standard'); ?>">Standard Specification</a></li>
						<li class="<?php echo $submenu == MENU_SPECIFICATION_OPTION ? 'active' : ''; ?>"><a href="<?php echo site_url('Specification/option_accessaries'); ?>">Option Accessaries</a></li>
					</ul>
				</li>
				<li id="" class="<?php echo $menu == MENU_DOWNLOAD ? 'active' : ''; ?>">
					<a href="<?php echo site_url('Download'); ?>" class="dropdown-toggle" role="button">Download</a>
				</li>
				<li id="" class="<?php echo $menu == MENU_LOCATION ? 'active' : ''; ?>">
					<a href="<?php echo site_url('Location'); ?>" class="dropdown-toggle" role="button">Location</a>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
		<?php } ?>
	<?php if($menu != MENU_HOME){ ?>
	</div>
	<?php } ?>
</div>