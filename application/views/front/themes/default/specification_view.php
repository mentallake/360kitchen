<div id="specification-standard-page">
	<div class="container">
		<div id="standard-panel" class="content-page-panel type-1">
			<div class="content-panel-header">
				<div class="row no-gap">
					<div class="col-xs-10">
						<ul class="page-menus hidden-xs">
							<li class="active"><a href="<?php echo site_url('Specification/standard'); ?>">Standard Specification</a></li>
							<li><a href="<?php echo site_url('Specification/option_accessaries'); ?>">Option Accessories</a></li>
						</ul>

						<ul id="spec-menu-dropdown" class="page-menus visible-xs">
							<li class="dropdown active">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Standard Specification</a>
								<ul class="dropdown-menu dropdown-menu-left">
									<li class="active"><a href="<?php echo site_url('Specification/standard'); ?>">Standard Specification</a></li>
									<li><a href="<?php echo site_url('Specification/option_accessaries'); ?>">Option Accessories</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-xs-2 text-right">
						<div id="" class="dropdown">
							<a id="spec-menu-btn" class="dot-menu-btn dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>

							<div id="spec-dropdown-menu" class="dropdown-menu dropdown-menu-right">
								<div class="row narrow">
									<?php
									for($i = 0; $i < count($items); $i++){
										$item = $items[$i];
										$item_id = $item['id'];
										$image_url = $item['image'] == "" ? uploads_url("default-image.png") : uploads_url($item['image']);
										$default_class = $item['image'] == "" ? "default-image" : "";
									?>
									<a class="spec-menu-item col-xs-12 col-sm-6" data-item-id="<?php echo $item_id; ?>">
										<div class="spec-menu-item-inner row no-gap">
											<div class="image-col col-xs-5">
												<div class="image-col-inner">
													<div class="item-image <?php echo $default_class; ?>" style="background-image: url(<?php echo $image_url; ?>);"></div>
												</div>
											</div>
											<div class="content-col col-xs-7">
												<div class="content-col-inner">
													<div class="title"><?php echo $item['name']; ?></div>
												</div>
											</div>
										</div>
									</a>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content-panel-body">
				<?php
				for($i = 0; $i < count($items); $i++){
					$item = $items[$i];
					$item_id = $item['id'];
					$image_url = $item['image'] == "" ? uploads_url("default-image.png") : uploads_url($item['image']);
					$default_class = $item['image'] == "" ? "default-image" : "";

					$image_col_class = ($i % 2 == 1) ? "col-sm-push-6" : "";
					$content_col_class = ($i % 2 == 1) ? "col-sm-pull-6" : "";
				?>
				<div class="spec-item" data-item-id="<?php echo $item_id; ?>">
					<div class="row no-gap">
						<div class="image-col col-sm-6 <?php echo $image_col_class; ?>">
							<div class="image-col-inner">
								<div class="item-image <?php echo $default_class; ?>" style="background-image: url(<?php echo $image_url; ?>);"></div>
							</div>
						</div>
						<div class="content-col col-sm-6 <?php echo $content_col_class; ?>">
							<div class="content-col-inner">
								<div class="title"><?php echo $item['name']; ?></div>
								<div class="content">
									<?php echo $item['description']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>