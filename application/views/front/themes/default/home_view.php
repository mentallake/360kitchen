<div id="home-page">
	<?php
	if ( $this->agent->referrer() ){
		$referer = $this->agent->referrer();
		$base_url = site_url();

		if(!strstr($referer, $base_url)){
	?>
	<div id="drag-mask">
		<img src="<?php echo assets_images_url('drag-icon.gif'); ?>" alt="">
	</div>
	<?php
		}
	}else{
	?>
	<div id="drag-mask">
		<img src="<?php echo assets_images_url('drag-icon.gif'); ?>" alt="">
	</div>
	<?php } ?>

	<div class="">
		<div class="nav-button-container hide">
			<a class="nav-button prev text-center"><i class="fa fa-angle-left"></i></a>
			<a class="nav-button next text-center"><i class="fa fa-angle-right"></i></a>
		</div>

		<div id="main-slide">
			<ul id="slide-paging">
				<?php
				for($i = 0; $i < count($kitchens); $i++){
					$current_class = $i == 0 ? "current" : "";
				?>
				<li class="<?php echo $current_class; ?>">
					<a data-slide-no="<?php echo $i; ?>"></a>
				</li>
				<?php } ?>
			</ul>

			<?php
			for($i = 0; $i < count($kitchens); $i++){
				$slide_id = "#slide-" . ($i + 1);
				$current_class = $i == 0 ? "current" : "";
				$kitchen = $kitchens[$i];
				$slide_image_url = uploads_url($kitchen['image']);
				$slide_title = $kitchen['title'];
				$slide_mask_title = str_replace("<br>", "", $slide_title);
				$slide_name = $kitchen['code_name'];
				$slide_description = $kitchen['description'];
				$view_360_url = site_url('Model/view_360/' . $kitchen['code_name']);
			?>
			<section data-role="section"
					 id="<?php echo $slide_id; ?>"
					 data-slide-no="<?php echo $i; ?>"
					 class="section <?php echo $current_class; ?>">
				<div class="section-inner"
					 style="background-image: url(<?php echo $slide_image_url; ?>);">
					<div class="mask">
						<div class="mask-title"><?php echo $slide_mask_title; ?></div>
					</div>
					<a href="">
						<img class="slide-logo" src="<?php echo assets_images_url('logo.png'); ?>" alt="">
					</a>
					<a class="detail-btn"
					   data-href="#slide-1"
					   data-kitchen-id="<?php echo $kitchen['id']; ?>">
						<img src="<?php echo assets_images_url('detail-btn-text.png'); ?>" alt="">
					</a>
					<div class="slide-name hidden-xs"><?php echo $slide_name; ?></div>
					<a href="<?php echo $view_360_url; ?>" class="btn-360"></a>

					<div class="slide-content">
						<div class="slide-title">
							<?php echo $slide_title; ?>
						</div>
						<div class="slide-name visible-xs"><?php echo $slide_name; ?></div>
						<div class="slide-description hidden-xs">
							<?php echo $slide_description; ?>
						</div>
					</div>
				</div>
			</section>
			<?php } ?>

			<div class="mobile-extra-slide visible-xs">
				<?php
				for($i = 0; $i < count($kitchens); $i++){
					$slide_id = "#slide-" . ($i + 1);
					$current_class = $i == 0 ? "current" : "";
					$kitchen = $kitchens[$i];
					$slide_image_url = uploads_url($kitchen['image']);
					$slide_title = $kitchen['title'];
					$slide_mask_title = str_replace("<br>", "", $slide_title);
					$slide_name = $kitchen['code_name'];
					$slide_description = $kitchen['description'];
				?>
				<section data-role="section"
						 id="<?php echo $slide_id; ?>"
						 data-slide-no="<?php echo $i; ?>"
						 class="section <?php echo $current_class; ?>"
						 style="background-image: url(<?php echo $slide_image_url; ?>);">
					<div class="section-inner">
						<div class="mask">
							<div class="mask-title"><?php echo $slide_mask_title; ?></div>
						</div>
					</div>
				</section>
				<?php } ?>
			</div>
		</div>
	</div>
</div>


<?php
for($i = 0; $i < count($kitchens); $i++){
	$kitchen = $kitchens[$i];
	$view_360_url = site_url('Model/view_360/' . $kitchen['code_name']);
?>
<div class="detail-mask-panel hide" data-kitchen-id="<?php echo $kitchen['id']; ?>">
	<div class="model-container">
		<div class="gallery-image-container">
			<div class="mobile-model-content-mask"></div>
			<div class="detail-text-mask"></div>

			<div class="mobile-model-content visible-xs">
				<div class="model-name">
					<?php echo $kitchen['code_name']; ?>
				</div>
				<div class="detail-content">
					<div class="detail-text">
						<?php echo $kitchen['detail_description']; ?>
					</div>
					<a class="expand-text-btn"></a><a class="fullscreen-btn">FULL GALLERY</a>
				</div>
				<div class="btn-360-wrapper">
					<a href="<?php echo $view_360_url; ?>" class="btn-360"></a>
				</div>
			</div>
		</div>

		<div class="kitchen-model-content hidden-xs">
			<div class="container">
				<div class="model-name hidden-xs">
					<?php echo $kitchen['code_name']; ?>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="gallery-label-wrapper">
							<div class="label-text hidden-xs">GALLERY</div>
						</div>
						<ul class="gallery-image-list owl-carousel">
							<?php
							$images = $kitchen['gallery'];

							for($ii = 0; $ii < count($images); $ii++){
								$image_url = uploads_url($images[$ii]['image']);
							?>
							<li class="item <?php echo $ii == 0 ? 'active' : ''; ?>">
								<a style="background-image: url(<?php echo $image_url; ?>);"></a>
							</li>
							<?php } ?>
						</ul>
					</div>
					<div class="col-sm-6 hidden-xs">
						<div class="detail-text">
							<?php echo $kitchen['detail_description']; ?>
						</div>
						<div class="btn-360-wrapper">
							<a href="<?php echo $view_360_url; ?>" class="btn-360 btn-red"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<div id="image-dialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="container vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>

				<?php
				for($i = 0; $i < count($kitchens); $i++){
					$kitchen = $kitchens[$i];
				?>
				<div class="kitchen-images hide" data-kitchen-id="<?php echo $kitchen['id']; ?>">
					<div class="model-name"><?php echo $kitchen['code_name']; ?></div>

					<div class="owl-carousel owl-theme">
						<?php
						$images = $kitchen['gallery'];

						for($ii = 0; $ii < count($images); $ii++){
							$image_url = uploads_url($images[$ii]['image']);
						?>
						<div class="item">
							<div class="img-wrapper">
								<img src="<?php echo $image_url; ?>" alt="">
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php }	 ?>
			</div>
		</div>
	</div>
</div>