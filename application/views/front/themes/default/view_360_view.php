<input type="hidden" id="kitchen-id" value="<?php echo $kitchen_id; ?>">

<div id="view-360-page"
	 class="view-360-page"
	 style="background-image: url(<?php echo $page_bg; ?>);">
	<div id="view-360-panel" class="content-page-panel">
		<div class="container">
			<div class="view-360-inner-panel">
				<div id="drag-screen-mask">
					<img src="<?php echo assets_images_url('drag-screen-icon.png'); ?>" alt="">
				</div>

				<div class="image-wrapper">
					<a class="btn-more-detail"></a>
					<div id="image-mask"></div>
					<img id="image" src="" alt="">
				</div>

				<div id="customize-panel">
					<div class="title-panel">
						<a id="customize-btn" class="title">Customize</a>
						<div class="subtitle">เลือกสีและวัสดุ</div>
					</div>

					<div class="component-list-container">
						<?php
						for($i = 0; $i < count($components); $i++){
							$component = $components[$i];
							$component_id = $component['id'];
							$title = $component['title'];
							$items = $component['items'];
						?>
						<div class="component-list-panel">
							<div class="title"><?php echo $title ?>:</div>
							<ul class="component-list"
								data-id="<?php echo $component_id; ?>">
								<?php
								for($ii = 0; $ii < count($items); $ii++){
									$item = $items[$ii];
									$item_id = $item['id'];
									$item_title = $item['title'];
									$item_image_url = $item['image'] == '' ? '' : uploads_url('components/' . $item['image']);
									$active_class = $ii == 0 ? 'active' : '';
								?>
								<li class="<?php echo $active_class; ?>"
									data-id="<?php echo $item_id; ?>">
									<a style="background-image: url(<?php echo $item_image_url; ?>);"></a>
									<div class="title"><?php echo $item_title; ?></div>
								</li>
								<?php } ?>
							</ul>
						</div>
						<?php } ?>
					</div>

					<div class="button-panel text-center">
						<a id="customize-confirm-btn" class="btn">Done</a>
					</div>
				</div>
			</div>
		</div>

		<div id="nav-panel">
			<a href="<?php echo $prev_model_url; ?>" class="nav-btn prev">
				<img src="<?php echo assets_images_url('left-arrow-icon.png'); ?>" alt=""><?php echo $prev_model_name; ?>
			</a>
			<a href="<?php echo $next_model_url; ?>" class="nav-btn next">
				<?php echo $next_model_name; ?><img src="<?php echo assets_images_url('right-arrow-icon.png'); ?>" alt="">
			</a>
		</div>
	</div>

	<div id="slider" class="hide"></div>
	<div id="swipe-text" class="hide" style="color: #fff;"></div>
</div>

<div id="images-playground" class="hide"></div>
<div id="detail-playground" class="hide"></div>

<div id="detail-dialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="container vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				<div class="content-panel">
				</div>
			</div>
		</div>
	</div>
</div>