<div id="location-page">
	<div class="container">
		<div id="location-panel" class="content-page-panel">
			<div class="content-panel-header">
				<div class="content-panel-title">Store Location</div>
			</div>
			<div class="content-panel-body">
				<div class="row">
					<?php
					for($i = 0; $i < count($locations); $i++){
						$location = $locations[$i];
					?>
					<div class="location-item-col col-sm-4">
						<div class="location-item">
							<div class="location-name"><?php echo $location['name']; ?></div>
							<div class="location-address"><?php echo $location['address']; ?></div>
							<div class="location-address-th"><?php echo $location['address_th']; ?></div>
							<div class="location-contact">
								<span class="label-text">Tel: </span><span class="location-tel"><?php echo $location['tel']; ?></span>
								<?php if($location['fax'] != ""){ ?>
								&nbsp;&nbsp;
								<span class="label-text">Fax: </span><span class="location-fax"><?php echo $location['fax']; ?></span>
								<?php } ?>
							</div>
							<?php if($location['map_url'] != ""){ ?>
							<a href="<?php echo $location['map_url']; ?>" target="blank" class="view-map-link"><i class="fa fa-map-marker"></i> View Map</a>
							<?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>