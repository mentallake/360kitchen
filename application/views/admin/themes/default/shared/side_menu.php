<nav id="main-navigator" class="navigator">
    <div class="sidebar-collapse">
        <a href="<?php echo site_url('admin'); ?>" id="logo"><img src="<?php echo assets_admin_images_url('logo.png'); ?>" alt=""></a>
        <ul class="side-menu lv-1-ul">
            <?php
            $active_class = $menu == MENU_ADMIN_KITCHEN ? 'active' : '';
            $collapse_class = $menu == MENU_ADMIN_KITCHEN ? 'collapse in' : 'collapse';
            ?>
            <li class="lv-1-li <?php echo $active_class; ?>">
                <a href="<?php echo site_url('admin/Kitchen'); ?>" title="จัดการ Kitchen">
                    <span class="menu-item-icon">
                        <img src="<?php echo assets_admin_images_url('k-icon.png'); ?>" alt="">
                    </span>
                    <span class="menu-item-title">Kitchen</span>
                </a>
            </li>
            <?php
            $active_class = $menu == MENU_ADMIN_SPECIFICATION ? 'active' : '';
            $collapse_class = $menu == MENU_ADMIN_SPECIFICATION ? 'collapse in' : 'collapse';
            ?>
            <li class="lv-1-li <?php echo $active_class; ?>">
                <a href="<?php echo site_url('admin/Specification'); ?>" title="จัดการ Standard Specification">
                    <span class="menu-item-icon">
                        <img src="<?php echo assets_admin_images_url('s-icon.png'); ?>" alt="">
                    </span>
                    <span class="menu-item-title">Standard Specification</span>
                </a>
            </li>
            <?php
            $active_class = $menu == MENU_ADMIN_ACCESSARIES ? 'active' : '';
            $collapse_class = $menu == MENU_ADMIN_ACCESSARIES ? 'collapse in' : 'collapse';
            ?>
            <li class="lv-1-li <?php echo $active_class; ?>">
                <a href="<?php echo site_url('admin/Accessaries'); ?>" title="จัดการ Option Accessaries">
                    <span class="menu-item-icon">
                        <img src="<?php echo assets_admin_images_url('o-icon.png'); ?>" alt="">
                    </span>
                    <span class="menu-item-title">Option Accessaries</span>
                </a>
            </li>
            <?php
            $active_class = $menu == MENU_ADMIN_DOWNLOAD ? 'active' : '';
            $collapse_class = $menu == MENU_ADMIN_DOWNLOAD ? 'collapse in' : 'collapse';
            ?>
            <li class="lv-1-li <?php echo $active_class; ?>">
                <a href="<?php echo site_url('admin/Download'); ?>" title="จัดการ Download">
                    <span class="menu-item-icon">
                        <img src="<?php echo assets_admin_images_url('d-icon.png'); ?>" alt="">
                    </span>
                    <span class="menu-item-title">Download</span>
                </a>
            </li>
        </ul>
    </div>
</nav>