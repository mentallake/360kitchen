<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo "เข้าสู่ระบบ | " . APP_NAME; ?></title>

		<link rel="stylesheet" type="text/css" href="<?php echo assets_admin_css_url( 'reset.min.css' ); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo assets_admin_css_url( 'animate.css' ); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo assets_admin_fonts_url( 'font-awesome-4.7.0/css/font-awesome.min.css' ); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo assets_admin_libs_url( 'bootstrap-3.3.7/css/bootstrap.min.css' ); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo assets_admin_libs_url( 'toastr/toastr.min.css' ); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo assets_admin_css_url( 'style.css' ); ?>">

        <script type="text/javascript" src="<?php echo assets_admin_libs_url( 'modernizr-custom.min.js' ); ?>"></script>
    </head>
    <body class="login-page">
        <div class="container">
            <div id="login-panel">
                <?php echo form_open( "admin/Login/perform_login" ); ?>

                <div class="logo">
                	<img src="<?php echo assets_images_url('logo.png'); ?>" alt="">
                </div>

                <div class="field-wrapper">
	                <div class="form-group">
	                    <input type="text" class="form-control required" name="username" placeholder="Username" required />
	                </div>
	                <div class="form-group">
	                    <input type="password" class="form-control required" name="password" placeholder="Password" required />
	                </div>
	                <div>
	                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-lock"></i>&nbsp;&nbsp;เข้าสู่ระบบ</a>
	                </div>
	            </div>

                <?php echo form_close(); ?>
            </div>
        </div>

		<script type="text/javascript" src="<?php echo assets_admin_libs_url( 'jquery-3.2.1.min.js' ); ?>"></script>
		<script type="text/javascript" src="<?php echo assets_admin_libs_url( 'bootstrap-3.3.7/js/bootstrap.min.js' ); ?>"></script>
        <script type="text/javascript" src="<?php echo assets_admin_scripts_url( 'js/login.js' ); ?>"></script>
    </body>
</html>