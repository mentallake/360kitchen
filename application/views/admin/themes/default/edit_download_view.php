<div id="edit-download-page" class="content-panel">
	<div class="page-header-panel container-fluid">
		<div class="row">
			<div class="col-xs-6">
				<div class="page-title">
					<?php echo $page_title; ?>
				</div>
			</div>
			<div class="col-xs-6 text-right">
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<?php
			$attributes = array('id' => 'download-form', 'class' => 'form');
			echo form_open_multipart(site_url('admin/Download/update'), $attributes);
		?>
			<input type="hidden" name="id" value="<?php echo $item['id']; ?>">

			<div class="row">
				<div class="col-xs-4">
					<div class="form-group text-center">
						<?php
						$has_image = $item['image'] == '' ? false : true;
						$has_image_class = $has_image ? 'has-image' : '';
						$url = $has_image ? uploads_url($item['image']) : assets_admin_images_url('default-image.png');
						?>
						<a id="download-image-upload-btn" class="download-image file-upload-wrapper image-container medium <?php echo $has_image_class; ?>" style="background-image: url('<?php echo $url; ?>');">
							<input type="file" class="file-upload" name="image" accept="image/*">
						</a>
						<div id="download-image-upload-text" class="upload-text">คลิกที่รูปภาพเพื่ออัพโหลด<br>(ขนาดรูปที่แนะนำ 275x380)</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="title-textbox">ชื่อ<span class="text-red">*</span></label>
								<input type="text" name="title" class="form-control clearable-textbox required" id="title-textbox" placeholder="" value="<?php echo $item['title']; ?>">
							</div>
						</div>
						<div class="col-xs-12">
							<?php
							$issue_date = timestamp_to_date($item['issue_date'], "d/m/Y");
							?>
							<div class="form-group">
								<label for="-textbox">วันที่ Issue<span class="text-red">*</span></label>
								<input type="text" name="issue_date" class="form-control clearable-textbox datepicker required" id="issue-date-textbox" placeholder="" value="<?php echo $issue_date; ?>" required>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label for="-textbox">ไฟล์</label>
								<input type="file" name="filename" class="form-control " id="filename-file-input" placeholder="" value="<?php echo $item['filename']; ?>">
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label for="status-select">สถานะ</label>
								<?php echo form_dropdown("status", $status_options, $item['status'], 'class="selectpicker form-control show-tick required" id="status-select"'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="button-panel text-center">
				<a href="<?php echo site_url('admin/Download'); ?>" class="btn btn-grey-1 btn-size-150">ยกเลิก</a>
				<button type="submit" class="btn btn-primary btn-size-150">บันทึก</button>
			</div>
		</form>
	</div>
</div>