<div id="edit-kitchen-page" class="content-panel">
	<div class="page-header-panel container-fluid">
		<div class="row">
			<div class="col-xs-6">
				<div class="page-title">
					<?php echo $page_title; ?>
				</div>
			</div>
			<div class="col-xs-6 text-right">
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<?php
			$attributes = array('id' => 'kitchen-form', 'class' => 'form');
			echo form_open_multipart(site_url('admin/Kitchen/update'), $attributes);
		?>
			<input type="hidden" name="id" value="<?php echo $item['id']; ?>">

			<div class="section-title">ข้อมูลห้องครัว</div>

			<div class="row">
				<div class="col-xs-5">
					<div class="form-group text-center">
						<?php
						$has_image = $item['image'] == '' ? false : true;
						$has_image_class = $has_image ? 'has-image' : '';
						$url = $has_image ? uploads_url($item['image']) : assets_admin_images_url('default-image.png');
						?>
						<a id="kitchen-image-upload-btn" class="kitchen-image file-upload-wrapper image-container medium <?php echo $has_image_class; ?>" style="background-image: url('<?php echo $url; ?>');">
							<input type="file" class="file-upload" name="image" accept="image/*">
						</a>
						<div id="kitchen-image-upload-text" class="upload-text">คลิกที่รูปภาพเพื่ออัพโหลด<br>(ขนาดรูปที่แนะนำ 1920x1280)</div>
					</div>
				</div>
				<div class="col-xs-7">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="title-textbox">ชื่อ<span class="text-red">*</span></label>
								<input type="text" name="code_name" class="form-control clearable-textbox required" id="title-textbox" placeholder="" value="<?php echo $item['code_name']; ?>">
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label for="title-textarea">หัวข้อ<span class="text-red">*</span></label>
								<textarea name="title" rows="4" class="form-control required" id="title-textarea" placeholder=""><?php echo $item['title']; ?></textarea>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label for="description-textarea">รายละเอียด<span class="text-red">*</span></label>
								<textarea name="description" rows="8" class="form-control required" id="description-textarea" placeholder=""><?php echo $item['description']; ?></textarea>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label for="status-select">สถานะ</label>
								<?php echo form_dropdown("status", $status_options, $item['status'], 'class="selectpicker form-control show-tick required" id="status-select"'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="section-title">ข้อมูล Model</div>

			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="detail-description-textarea">รายละเอียดในหน้า Gallery<span class="text-red">*</span></label>
						<textarea name="detail_description" rows="7" class="form-control required" id="detail-description-textarea" placeholder=""><?php echo $item['detail_description']; ?></textarea>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<input type="hidden" id="remove-image-ids" name="remove_image_ids" value="">

						<label for="description-textarea">รูป (ขนาดรูปที่แนะนำ 1920x1280)</label>
						<ul id="gallery-image-list" class="row narrow">
							<li class="col-sm-3">
								<a id="add-image-btn" class="gallery-image-btn file-upload-wrapper">
									<input type="file" class="file-upload" name="gallery_images[]" accept="image/*" multiple>

									<div class="vertical-middle-parent">
										<div class="vertical-middle-child">
											<img src="<?php echo assets_admin_images_url('image-icon.png'); ?>" alt="">
											<div class="title">เพิ่มรูป</div>
										</div>
									</div>
								</a>
							</li>

							<?php
							$gallery = $item['gallery'];
							$number = 0;

							for($i = 0; $i < count($gallery); $i++){
								$image_id = $gallery[$i]['id'];
								$image_url = uploads_url($gallery[$i]['image']);
								$image_order = $gallery[$i]['image_order'];
								$number++;
							?>
							<li class="col-sm-4 col-md-3 gallery-image-item" data-id="<?php echo $image_id; ?>">
								<input type="hidden" name="gi_id_<?php echo $i; ?>" value="<?php echo $image_id; ?>">
								<input type="hidden" class="gi-order-field" name="gi_order_<?php echo $i; ?>" value="<?php echo $image_order; ?>">

								<div class="gallery-image">
									<a class="remove-image btn skelleton"><i class="fa fa-trash"></i></a>
									<div class="image-container" style="background-image: url(<?php echo $image_url; ?>);"></div>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="button-panel text-center">
				<a href="<?php echo site_url('admin/Kitchen'); ?>" class="btn btn-grey-1 btn-size-150">ยกเลิก</a>
				<button type="submit" class="btn btn-primary btn-size-150">บันทึก</button>
			</div>
		</form>
	</div>
</div>