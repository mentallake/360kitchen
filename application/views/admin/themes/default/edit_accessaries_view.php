<div id="edit-accessaries-page" class="content-panel">
	<div class="page-header-panel container-fluid">
		<div class="row">
			<div class="col-xs-6">
				<div class="page-title">
					<?php echo $page_title; ?>
				</div>
			</div>
			<div class="col-xs-6 text-right">
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<?php
			$attributes = array('id' => 'accessaries-form', 'class' => 'form');
			echo form_open_multipart(site_url('admin/Accessaries/update'), $attributes);
		?>
			<input type="hidden" name="id" value="<?php echo $item['id']; ?>">

			<div class="row">
				<div class="col-xs-5">
					<div class="form-group text-center">
						<?php
						$has_image = $item['image'] == '' ? false : true;
						$has_image_class = $has_image ? 'has-image' : '';
						$url = $has_image ? uploads_url($item['image']) : assets_admin_images_url('default-image.png');
						?>
						<a id="accessaries-image-upload-btn" class="accessaries-image file-upload-wrapper image-container medium <?php echo $has_image_class; ?>" style="background-image: url('<?php echo $url; ?>');">
							<input type="file" class="file-upload" name="image" accept="image/*">
						</a>
						<div id="accessaries-image-upload-text" class="upload-text">คลิกที่รูปภาพเพื่ออัพโหลด<br>(ขนาดรูปที่แนะนำ 1140x900)</div>
					</div>
				</div>
				<div class="col-xs-7">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="title-textbox">ชื่อ<span class="text-red">*</span></label>
								<input type="text" name="name" class="form-control clearable-textbox required" id="title-textbox" placeholder="" value="<?php echo $item['name']; ?>">
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label for="description-textarea">รายละเอียด<span class="text-red">*</span></label>
								<textarea name="description" rows="10" class="form-control editor" id="description-textarea" placeholder=""><?php echo $item['description']; ?></textarea>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label for="status-select">สถานะ</label>
								<?php echo form_dropdown("status", $status_options, $item['status'], 'class="selectpicker form-control show-tick required" id="status-select"'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="button-panel text-center">
				<a href="<?php echo site_url('admin/Accessaries'); ?>" class="btn btn-grey-1 btn-size-150">ยกเลิก</a>
				<button type="submit" class="btn btn-primary btn-size-150">บันทึก</button>
			</div>
		</form>
	</div>
</div>