<div id="kitchen-page" class="content-panel">
	<div class="page-header-panel container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<div class="page-title">
					<?php echo $page_title; ?>
				</div>
			</div>
			<div class="col-xs-8 text-right">
				<a href="<?php echo site_url('admin/Kitchen/edit'); ?>" class="btn btn-primary action-button hide">
					<i class="fa fa-plus"></i>&nbsp;&nbsp;เพิ่มข้อมูลใหม่
				</a>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="table-actions">
			<ul>
				<li class="hide"><a href="" class="btn table-action-btn action-btn delete-btn"><i class="fa fa-trash-o"></i> ลบ</a></li>
			</ul>
		</div>
		<table id="kitchen-table" class="table data-table">
			<thead>
				<tr>
					<th width="40px;"><input type="checkbox" class="all-items-checkbox" value=""></th>
					<th width="50px;">No.</th>
					<th class="" width="100px;">รูป</th>
					<th class="">ชื่อ</th>
					<th class="">สถานะ</th>
					<th class="">วันที่แก้ไขล่าสุด</th>
					<th class=""></th>
				</tr>
			</thead>
		</table>
	</div>
</div>